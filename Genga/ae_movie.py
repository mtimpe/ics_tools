#!/usr/bin/env python

"""
Plot Semi-Major Axis vs. Eccentricity. Scale Point Size by Mass.
"""

import sys
sys.path.insert(0, "/home/ics/mtimpe/src/G3/Helpers")
import matplotlib as mpl; mpl.use('agg')
import matplotlib.pyplot as plt
import io_helpers as ioh
import glob
import argparse
import other_helpers as oh
import numpy as np

# Parse Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--run_name', default='mmsn', \
                    help='Name of Simulation Run.')
parser.add_argument('--dir_name', default='.', \
                    help='Directory')
args = parser.parse_args()

# Glob Outputs
nsteps = []
globs = glob.glob("%s/Out*.dat" % args.dir_name)
globs = sorted(globs)
for g in globs:
    nstep = int(g.strip()[:-4].split("_")[-1])
    nsteps.append(nstep)

# #############################################################################
# Find Smallest/Largest Mass
# #############################################################################

# 0 - Smallest Mass from First Output
df = ioh.read_output("%s/Out_%s_%012d.dat" % (args.dir_name, \
                                              args.run_name, \
                                              nsteps[-1]), \
                     frame="heliocentric")

mmin = df["mass"].min()

# # 1 - Largest Mass from Last Output
mmax = np.zeros(2)
df = ioh.read_output("Out_%s_%012d.dat" % (args.run_name, nsteps[-1]), \
                     frame="heliocentric")
#df = df[df.mass<16.0] 
mmax[0] = df["mass"].max()
lastmmax = mmax[0]
del df

# # 1 - Largest mass from all outputs
print "// Finding mmax"
# global mass maximum
globalmmax = 0.
globalmtime = 0
# global eccentricity maximum
globalemax = 0.
#globaletime = 0
# global semi-major axis maximum
globalamax = 0.
#globalatime = 0.
# loop through outputs to find global maximums
for nstep in nsteps:
	# Load
	df = ioh.read_output("%s/Out_%s_%012d.dat" % (args.dir_name, args.run_name, nstep), frame="heliocentric")
	# local mass max
	localmmax = df["mass"].max()
	# local eccentricity max
	localemax = df["e"].max()
	# local semi-major axis max
	localamax = df["a"].max()
	print "local maximums...\n\tm: {:.5f}\n\te: {:.5f}\n\ta: {:.5f}".format(localmmax, localemax, localamax)
	
	# local vs global check
	if localmmax > globalmmax:
		globalmmax = localmmax
		globalmtime = nstep

	if localemax > globalemax:
		globalemax = localemax
		#globaletime = localetime

	if localamax > globalamax:
		globalamax = localamax

	del df

mmax[0] = globalmmax

print "\nglobal maximums...\n\tm: {:.5f}\n\te: {:.5f}\n\ta: {:.5f}\n".format(globalmmax, globalemax, globalamax)

# Set Scaling (Smallest/Largest)
#m, n = oh.mkline(mmin, 1.0, np.nanmax(globalmmax), 36.0)

# Set Scaling (Smallest/One-Earth Mass)
m, n = oh.mkline(mmin, 1.0, 10.0, 36.0)

# #############################################################################
# Loop/Plot
# #############################################################################
fig, ax = plt.subplots(1,1)
print "// Looping Outputs"
for nstep in nsteps:
    # Load
    print "// %012d/%012d" % (nstep, nsteps[-1])
    df = ioh.read_output("%s/Out_%s_%012d.dat" % (args.dir_name, \
                                                  args.run_name, \
                                                  nstep), \
                         frame="heliocentric")
    s = df.mass * m + n
    dfx = df.sort(columns=["mass"], ascending=False).head(3)

    # Plot/Style
    ax.scatter(df.a, df.e, s=s**2.0, c="b", edgecolor="none", alpha=0.5)
    ax.set_title("%s/%s / %012d / %.2e yr" % (args.dir_name, \
                                              args.run_name, \
                                              nstep, \
                                              df.time.iloc[0]), \
                 fontsize="x-small")
    mtxt_tmp = ""
    if len(dfx) >= 1:
        mtxt_tmp += "%.2f" % dfx.iloc[0].mass
    if len(dfx) >= 2:
        mtxt_tmp += ", %.2f" % dfx.iloc[1].mass
    if len(dfx) >= 3:
        mtxt_tmp += ", %.2f" % dfx.iloc[2].mass
    mtxt = "Most Massive Planets: (%s)" % mtxt_tmp
    ax.text(0.05, 0.95, \
            mtxt, \
            horizontalalignment='left', \
            verticalalignment='top', \
            transform=ax.transAxes)
    ax.set_xlim([0,3])
    ax.set_ylim([0,globalemax*1.1])
    ax.set_xlabel("Semi-Major Axis (AU)")
    ax.set_ylabel("Eccentricity")

    # Save
    fig.savefig("ae_%012d.png" % nstep)

    # Clean
    del df
    plt.cla()

del fig

print "// Done"
