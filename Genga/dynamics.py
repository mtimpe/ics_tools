#!/usr/bin/env python

import os
import sys
import glob
#import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
#import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
sys.path.insert(0, "/home/ics/mtimpe/src/G3/Helpers")
import io_helpers as ioh
import source_helpers as fh
import other_helpers as oh
import kepler_helpers as kh
from palettable.tableau import Tableau_20


__author__ = "Miles Timpe"
__copyright__ = "Copyright 2016, Miles Timpe"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"


# Figure Parameters
rlim = 3
fs = 16

# Color Palette
cpalette = Tableau_20.hex_colors

# Glob Outputs
globs = glob.glob("Out*.dat")
globs = sorted(globs)

steps = [int(output.strip()[:-4].split("_")[-1]) for output in globs]

# Load all outputs
df = ioh.read_output_and_stack(globs, frame='heliocentric')

# Global min/max mass
mmin = df["mass"].min()
mmax = df["mass"].max()

# Final Planets
final   = ioh.read_output_and_stack([globs[-1]], frame='heliocentric')
planets = final.sort_values(by=["mass"], ascending=False).head(5)
pids    = list(planets["pid"])

amax = planets["a"].max()
imax = planets["i"].max()

# Create color dict
cdict = {}
for idx, id in enumerate(pids):
  cdict[id] = cpalette[idx]

# Main Loop
for idx, age in enumerate(df["time"].unique()):
  df1 = df[df["time"] == age]
  df2 = df[df["time"] <= age]

  # Calculate orbital ellipses
  orbit = df1[df1["mass"] >= 1.0]
  if len(orbit) > 0:
	orbit["ellipse"] = orbit.apply(lambda row: \
		kh.compute_ellipse(row["a"], row["e"], \
		row["i"], row["Omega"], row["omega"]), axis=1)

  # Marker scaling
  m, n = oh.mkline(mmin, 2.0, mmax, 10.0)
  s = df1.mass * m + n

  # Figure
  fig = plt.figure(figsize=(20, 10), facecolor="white")

  plt.rc('text', usetex=True)
  plt.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

  gs = gridspec.GridSpec(6, 2)  #, height_ratios=[2,1])

  gs1 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[:, 0], \
  	  wspace=0.0, hspace=0.0)

  gs2 = gridspec.GridSpecFromSubplotSpec(5, 1, subplot_spec=gs[1:, 1], \
  	  hspace=0.0)

  gs3 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs[0, 1], \
	  width_ratios=[1,5])

  # 3D Axis
  ax0 = plt.subplot(gs1[0], projection='3d')
  #ax0.view_init(30, 0)

  # Orbital Elements Axis
  ax1 = plt.subplot(gs2[0, 0])
  ax2 = plt.subplot(gs2[1, 0])
  ax3 = plt.subplot(gs2[2, 0])
  ax4 = plt.subplot(gs2[3, 0])
  ax5 = plt.subplot(gs2[4, 0])

  # Text Axis
  p1 = plt.subplot(gs3[0, 0], polar=True)
  p2 = plt.subplot(gs3[0, 1])

  # 3D Orbit
  ax0.scatter([0.], [0.], [0.], c="orange", marker=".", s=200, edgecolor="orange")
  ax0.scatter(df1["x"], df1["y"], df1["z"], c="black", \
  	  marker=".", s=s**2., edgecolor="black")

  # Plot Ellipses
  if len(orbit) > 0:
	for index, row in orbit.iterrows():
	  element = row["ellipse"]
	  if row["pid"] in cdict.keys():
	  	col = cdict[row["pid"]]
	  else:
	  	col = "black"
	  ax0.plot(element[0], element[1], element[2], color=col)

  ax0.set_xlim(-rlim, rlim)
  ax0.set_ylim(-rlim ,rlim)
  ax0.set_zlim(-rlim, rlim)

  # Time Wedge
  p1.axis("off")
  p1.set_rlim(0.5)
  p1.set_theta_direction(1)
  p1.set_theta_offset(np.pi/2.0)
  theta = 360.0 * float(idx+1) / float(len(steps))
  p1.add_artist(patches.Wedge((.5,.5), 0.4, 0, theta, width=0.05, \
  	  transform=p1.transAxes, color='Red', alpha=1.0))
  p1.text(0.5, 0.5, "{:.2f}".format(age*1.0e-6), \
  	  horizontalalignment="center", \
  	  verticalalignment="center", \
  	  fontsize=22)

  # Mass Bar Chart
  df3 = df1[df1["pid"].isin(pids)]
  df4 = df3.sort_values(by=["pid"], ascending=True)
  ids = list(df4["pid"])
  xbar = df4["mass"]
  ybar = np.arange(len(ids))

  cols = []
  for foo in ids:
  	cols.append(cdict[foo])

  p2.barh(ybar, xbar, align="center", color=cols, edgecolor="white", alpha=0.5)
  p2.set_xlim(0, 20)
  p2.set_yticks(ybar)
  p2.set_yticklabels(ids, fontsize=10)
  p2.invert_yaxis()
  for side in ["left", "top", "bottom", "right"]:
	p2.spines[side].set_visible(False)

  p2.xaxis.set_ticks_position('none')
  p2.yaxis.set_ticks_position('none')
  p2.set_xticklabels([])

  for ndx, mass in enumerate(df4["mass"]):
	p2.text(mass+0.3, ndx+0.15, r"{:.2f} M$_{{\oplus}}$".format(float(mass)), \
		horizontalalignment="left", \
		verticalalignment="center", \
		fontsize=10)

  for axis in [ax1, ax2, ax3, ax4]:
	plt.setp(axis.get_xticklabels(), visible=False)

  # Dynamics
  for cdx, pid in enumerate(pids):
	dpid = df2[df2["pid"] == pid]
	ax1.plot(dpid["time"], dpid["a"], \
		color=cdict[pid], alpha=0.5)
	ax2.plot(dpid["time"], dpid["e"], \
		color=cdict[pid], alpha=0.5)
	ax3.plot(dpid["time"], np.rad2deg(dpid["i"]), \
		color=cdict[pid], alpha=0.5)
	ax4.plot(dpid["time"], np.rad2deg(dpid["omega"]), \
		color=cdict[pid], alpha=0.5)
	ax5.plot(dpid["time"], np.rad2deg(dpid["Omega"]), \
		color=cdict[pid], alpha=0.5)

  ax1.set_ylim(0, amax+1.)
  ax1.set_ylabel(r"$a$", fontsize=fs, \
  	  labelpad=20, rotation='horizontal')

  ax2.set_ylim(0, 1)
  ax2.set_ylabel(r"$e$", fontsize=fs, \
  	  labelpad=20, rotation='horizontal')

  ax3.set_ylim(0, np.rad2deg(imax)+5.)
  ax3.set_ylabel(r"$i$", fontsize=fs, \
  	  labelpad=20, rotation='horizontal')

  ax4.set_ylim(0, 360) #2.*np.pi)
  ax4.set_ylabel(r"$\omega$", fontsize=fs, \
  	  labelpad=20, rotation='horizontal')

  ax5.set_ylim(0, 360) #2.*np.pi)
  ax5.set_ylabel(r"$\Omega$", fontsize=fs, \
  	  labelpad=20, rotation='horizontal')

  # Thicker axes
  for axis in [ax1, ax2, ax3, ax4, ax5]:
	[j.set_linewidth(3) for j in axis.spines.itervalues()]
	axis.tick_params(axis='both', which='major', pad=10)
	axis.tick_params('both', length=5, width=2, which='major', labelsize=fs)
	axis.tick_params('both', length=3, width=1, which='minor')
	axis.set_xlim(0, 10e6)

  ax5.set_xticks([0, 1e6, 2e6, 3e6, 4e6, 5e6, 6e6, 7e6, 8e6, 9e6, 10e6])
  ax5.set_xticklabels([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], fontsize=fs)
  ax5.set_xlabel("Time (Myr)", fontsize=fs)

  ax1.set_yticks([0, 1, 2, 3, 4, 5])
  ax1.set_yticklabels([0, 1, 2, 3, 4, 5], fontsize=fs)

  ax2.set_yticks([0.2, 0.4, 0.6, 0.8])
  ax2.set_yticklabels([0.2, 0.4, 0.6, 0.8], fontsize=fs)

  ax3.set_yticks([2, 6, 10, 14])
  ax3.set_yticklabels([2, 6, 10, 14], fontsize=fs)

  ax4.set_yticks([90, 180, 270])
  ax4.set_yticklabels([90, 180, 270], fontsize=fs)

  ax5.set_yticks([90, 180, 270])
  ax5.set_yticklabels([90, 180, 270], fontsize=fs)

  plt.savefig("f{:>05}.png".format(idx), \
  	  format='png', dpi=200)

  del df1
  del df2
  plt.close()

os.system("ffmpeg -r 12 -f image2 -s 640x480 -i f%05d.png \
	-vcodec libx264 -crf 25  -pix_fmt yuv420p test.mp4")

