#!/usr/bin/env python

import os
import subprocess
import sys
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
sys.path.insert(0, "/home/ics/mtimpe/src/G3/Helpers")
import io_helpers as ioh
import source_helpers as fh
import other_helpers as oh
import kepler_helpers as kh
from palettable.tableau import Tableau_20


__author__ = "Miles Timpe"
__copyright__ = "Copyright 2016, Miles Timpe"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"


# Working Directory
wdir = "/zbox/user/mtimpe/projects/drazkowska/simulations/A0M060N2noG"

# Figure Parameters
rlim = 2.7

# Color Palette
cpalette = Tableau_20.hex_colors

# Runs
dirs = glob.glob("{}/r0*".format(wdir))

# Timesteps
globs = glob.glob("{}/Out*.dat".format(dirs[0]))
globs = sorted(globs)
steps = [int(output.strip()[:-4].split("_")[-1]) for output in globs]

# Only first and last steps
#steps = steps[3489:]

# Setup Grid
ni = int(np.sqrt(len(dirs)))
nj = ni + 1
if nj * nj < len(dirs):
  ni += 1

# Primary Loop
for jdx, step in enumerate(steps):
  # Tracker
  print("{:0>8}/{}".format(step, steps[-1]))

  if jdx < 3489:
  	continue

  # Setup Figure
  fig = plt.figure(figsize=(16,16))

  # Secondary Loop
  for idx, dir in enumerate(dirs):
	run  = os.path.basename(dir)
	nout = subprocess.check_output("ls -l {}/Out*.dat | wc -l"\
		.format(dir), shell=True)
	#print "{:>03}\t{}\t{}".format(idx+1, run, nout)

	# GridID
	if int(run[-2:]) < 16:
	  name = "A0M060N2noG{}".format(run)
	else:
	  name = "ng{}".format(run[-3:])

	# Last Output
	last = "{}/{}/Out_{}_{:0>12}.dat"\
		.format(wdir, run, name, steps[-1])

	# All bodies more massive than Earth
	dff  = ioh.read_output(last, frame="heliocentric")
	dff  = dff[dff["mass"] >= 1.0]
	pids = list(dff["pid"])

	# (or) all bodies
	#dff  = ioh.read_output(last, frame="heliocentric")
	#pids = list(dff["pid"])

	# Create color dictionary
	cdict = {}
	for cdx, id in enumerate(pids):
	  cdict[id] = cpalette[cdx]

	# Load current output
	fout = "{}/{}/Out_{}_{:0>12}.dat"\
		.format(wdir, run, name, step)
	df1 = ioh.read_output(fout, frame="heliocentric")

	# Calculate orbital ellipses
	orbit = df1[df1["pid"].isin(pids)]
	orbit["ellipse"] = orbit.apply(lambda row: \
		kh.compute_ellipse(row["a"], row["e"], \
		row["i"], row["Omega"], row["omega"]), axis=1)

	# Marker scaling
	m, n = oh.mkline(0.01, 2.0, 20.0, 10.0)
	s = df1.mass * m + n

	# 3D Axis
	axis = fig.add_subplot(ni, nj, idx+1, projection="3d")
	#ax0.view_init(30, 0)

	axis.set_xlim(-rlim, rlim)
	axis.set_ylim(-rlim ,rlim)
	axis.set_zlim(-rlim, rlim)

	#axis.set_title("{}".format(run[-3:]))
	axis.axis("off")

	# 3D Orbit
	axis.scatter([0.], [0.], [0.], c="orange", marker=".", \
		s=100, edgecolor="orange")
	axis.scatter(df1["x"], df1["y"], df1["z"], c="black", \
		marker=".", s=s**2., edgecolor="black")

	# Scale ellipses colors and linewidth to mass
	mc, nc = oh.mkline(mmin, 0.1, 20.0, 3.0)

	# Plot Ellipses
	for index, row in orbit.iterrows():
	  lw = row["mass"] * mc + nc
	  element = row["ellipse"]
	  axis.plot(element[0], element[1], element[2], \
	  	  color=cdict[row["pid"]], linewidth=2)

	del dff
	del df1
	del orbit

  plt.subplots_adjust(wspace=0, hspace=0)

  plt.savefig("{}/frames/g{:0>6}.png".format(wdir, jdx), \
  	  format='png', dpi=100)
  plt.close()

os.system("ffmpeg -r 12 -f image2 -s 640x480 -i {}/frames/g%06d.png \
	-vcodec libx264 -crf 25 -pix_fmt yuv420p \
	/home/cluster/mtimpe/movies/grid.mp4".format(wdir))

