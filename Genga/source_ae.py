#!/usr/bin/env python

''' Uses first and last output of Genga run to visualize the source distribution
    for the N most massive final particles. User can specify for how many of the
    final particles to build source trees and whether or not to include a kernal
    density estimate of the source regions.

    Requires NumPy, Pandas, matplotlib, palettable, and seaborn python modules as
    well as a number of Volker's G3 helper modules.
'''

import sys
sys.path.insert(0, "/home/cluster/mtimpe/data/src/G3/Helpers")
import time
import glob
import argparse
import numpy as np
import pandas as pd
import seaborn as sns
import io_helpers as ioh
import formation_helpers as fh
import other_helpers as oh
import matplotlib.pyplot as plt
from palettable.colorbrewer.qualitative import Paired_5
from matplotlib.collections import LineCollection


__author__ = "Miles Timpe"
__copyright__ = "Copyright 2016, Miles Timpe"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"


# Parse Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--data', \
	default='./', help='Path to input files')
parser.add_argument('--N', default=5, \
	help='Top N most massive particles')
parser.add_argument('--start', default=0, type=int, \
	help='Start from this step')
parser.add_argument('--kde', default=True, \
	help='Include KDE overlay of source region?')
args = parser.parse_args()

# color palette
cpalette_name = "colorbrewer.qualitative.Paired_5"
source_c = Paired_5.hex_colors

# run name
run = glob.glob(args.data + '/Out_*.dat')[0].split('_')[1]
figname = str(run) + "_sources"

# Load first output
fn = "%s/Out_%s_%012d.dat" % (args.data, run, args.start)
di = ioh.read_output_and_stack([fn], frame='heliocentric')

# Determine final step
steps = []
outputs = glob.glob(args.data + '/Out_*.dat')
for file in outputs:
  foo = file.split('_')[2]
  bar = int(foo.split('.')[0])
  steps.append(bar)
fstep = int(max(steps))

# Load final output
fn = "%s/Out_%s_%012d.dat" % (args.data, run, fstep)
df = ioh.read_output_and_stack([fn], frame='heliocentric')

# min-max planet mass
mmin = di["mass"].min()
mmax = df["mass"].max()

# Load Collisions
fn = "%s/Collisions_%s.dat" % (args.data, run)
dc = ioh.read_collisions_and_stack([fn])

# Select N most massive planets
dm = df.sort_values(by=["mass"], ascending=False).head(args.N)
#print(dm.mass)

dN = pd.DataFrame()
for i in range(1, args.N+1, 1):
  tPID = int(dm.iloc[-i].pid)
  dN = dN.append(df[df["pid"] == tPID])

pids = np.asarray(dN.pid, dtype=np.int64)

# Build source trees for N PIDs
sources = {}
for idx, pid in enumerate(pids):
  foo = fh.return_sources(pid, dc)
  sources[pid] = foo

# Set Scaling
m, n = oh.mkline(mmin, 1.0, mmax, 26.0)
# Marker Sizes
di['s'] = di.mass * m + n
df['s'] = df.mass * m + n

# bin mass by semi-major axis
# Bin the data frame by "a" with 10 bins...
bins = 1000
bi = np.linspace(di.a.min(), di.a.max(), bins)
bf = np.linspace(df.a.min(), df.a.max(), bins)

#bins = np.logspace(np.log10(di.a.min()), np.log10(di.a.max()), 100)
#groups = df.groupby(pd.cut(di.a, bins))
gi = di.groupby(pd.cut(di.a, bi))
gf = df.groupby(pd.cut(df.a, bf))

# bin mids
midi = []
midf = []
for idx in range(1, len(bi)):
  midi.append((bi[idx]+bi[idx-1])/2.0)
  midf.append((bf[idx]+bf[idx-1])/2.0)

# Get the mean of b, binned by the values in a
mitotal = di.mass.sum()
mftotal = di.mass.sum()

offset = 1.05
hi = gi.sum().mass / mitotal
hi = (10 * hi) + offset
hf = gf.sum().mass / mftotal
hf = (10 * hf) + offset

# Figures
sns.set_style('whitegrid')

fig, ax = plt.subplots(1, 1, figsize=(11.7, 8.3))

# Thicker axes
[i.set_linewidth(3) for i in ax.spines.itervalues()]
ax.tick_params(axis='both', which='major', pad=10)
ax.tick_params('both', length=10, width=3, which='major', labelsize=22)
ax.tick_params('both', length=5, width=2, which='minor')

# Offset initial output (source region)
yi = di.e + 1.225

# Plot a-e for initial and final outputs
ax.scatter(di.a, yi, s=di.s**2.0, color="black", edgecolor="none", zorder=1)
ax.scatter(df.a, df.e, s=df.s**2.0, color="black", edgecolor="none", zorder=1)

#ki = sns.kdeplot(np.array(di.a))
#print ki

for idx, pid in enumerate(pids):
  slist = sources[pid]

  # Highlight Source Bodies
  dsi = di[di['pid'].isin(slist)]
  #ssi = dsi.mass * m + n
  ysi = dsi.e + 1.225
  pidc = source_c[idx]
  ax.scatter(dsi.a, ysi, s=10, color=pidc, edgecolor="none", zorder=2)

  # KDE
  if args.kde == True:
  	sns.kdeplot(np.array(dsi.a), color=pidc, alpha=0.5)

  # Highlight Final Body
  dsf = df[df['pid'] == pid]
  ssf = dsf.mass * m + n + 1
  #ysf = dsf.e
  ax.scatter(dsf.a, dsf.e, s=ssf**2.0, color=pidc, edgecolor="none", zorder=2)

#ax.set_title('Final Configuration')
ax.set_xlabel('Semi-Major Axis (AU)', fontsize=22)
ax.set_ylabel('Eccentricity', fontsize=22)
ax.set_xscale('log')
ax.set_xlim(0.5, 40)
ax.set_ylim(-0.05, 1.3)
#ax.set_xticks([0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30])
#ax.set_xticklabels([0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30])
ax.set_xticks([1, 10, 40])
ax.set_xticklabels([1, 10, 40])
ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.set_yticklabels([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
ax.grid(False)

plt.savefig('{}.png'.format(figname), \
	format='png')
<<<<<<< HEAD:Genga/source_ae.py
plt.show()
=======
#plt.show()
>>>>>>> aed1c49ac8d1a92777822cf777f6e3580c0491a6:source_ae.py
