#!/usr/bin/env python

import sys
import glob
import argparse
import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
sys.path.insert(0, "/home/ics/mtimpe/src/G3/Helpers")
import io_helpers as ioh
import source_helpers as fh
import other_helpers as oh
#import seaborn as sns
#from palettable.colorbrewer.qualitative import Paired_3
#from matplotlib.collections import LineCollection


__author__ = "Miles Timpe"
__copyright__ = "Copyright 2016, Miles Timpe"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"


# Glob Outputs
globs = glob.glob("Out*.dat")
globs = sorted(globs)
steps = [int(output.strip()[:-4].split("_")[-1]) for output in globs]

initial = globs[0]
final   = globs[-1]

# Load Initial Output
dfi = ioh.read_output_and_stack([initial], frame='heliocentric')

# Pad IC time for logarithmic x-axis
#dfi.time = dfi.time + 1.0

# Load Final Output
dff = ioh.read_output_and_stack([final], frame='heliocentric')

# Minimum mass
mmin = min(dfi["mass"].min(), dff["mass"].min())
# Maximum mass
mmax = max(dfi["mass"].max(), dff["mass"].max())

# Load Collisions
#fname = "Collisions_%s.dat" % args.run_name
#dfc = ioh.read_collisions_and_stack([fname])

# Figure Parameters
rlim = 3
rsun = 200
fs=18

for i, frame in enumerate(globs):
  print frame
  df = ioh.read_output_and_stack([frame], frame='heliocentric')

  m, n = oh.mkline(mmin, 2.0, mmax, 10.0)
  s = df.mass * m + n

  # Figure
  fig = plt.figure(figsize=(16, 8), facecolor="white")

  gs = gridspec.GridSpec(1, 2)

  gs1 = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=gs[0], \
  	  wspace=0.0, hspace=0.0)

  ax2 = plt.subplot(gs1[1:,:-1])
  ax2.set_aspect("equal")

  ax1 = plt.subplot(gs1[0, :-1], sharex=ax2)
  ax3 = plt.subplot(gs1[1:, -1], sharey=ax2)

  axt = plt.subplot(gs1[0, 2], polar=True)
  axt.set_rlim(0.5)
  axt.axis("off")
  #circle = plt.Circle((0.0, 0.0), 0.2, transform=axt.transData._b, color="red")
  #axt.add_artist(circle)
  axt.add_artist(patches.Wedge((.5,.5), 0.3, 0, 215, width=0.1, transform=axt.transAxes, color='blue', alpha=0.5))

  ax1.scatter([0.0], [0.0], color='orange', marker='.', s=rsun, \
  	  edgecolor=None, zorder=1)
  ax2.scatter([0.0], [0.0], color='orange', marker='.', s=rsun, \
  	  edgecolor=None, zorder=1)
  ax3.scatter([0.0], [0.0], color='orange', marker='.', s=rsun, \
  	  edgecolor=None, zorder=1)

  ax1.scatter(df["x"], df["z"], color='k', marker='.', s=s**2., \
  	  edgecolor=None, zorder=5)
  ax2.scatter(df["x"], df["y"], color='k', marker='.', s=s**2., \
  	  edgecolor=None, zorder=5)
  ax3.scatter(df["z"], df["y"], color='k', marker='.', s=s**2., \
  	  edgecolor=None, zorder=5)

  ax1.set_ylabel("z", fontsize=fs)

  ax2.set_xlabel("x", fontsize=fs)
  ax2.set_ylabel("y", fontsize=fs)

  ax3.set_xlabel("z", fontsize=fs)

  ax1.set_xlim(-rlim,rlim)
  ax1.set_ylim(-1,1)

  ax2.set_xlim(-rlim,rlim)
  ax2.set_ylim(-rlim,rlim)

  ax3.set_xlim(-1,1)
  ax3.set_ylim(-rlim,rlim)

  ax1.set_xticklabels([])
  ax1.set_yticks([-0.5, 0.0, 0.5])
  ax1.set_yticklabels([-0.5, 0.0, 0.5])

  ax3.set_xticks([-0.5, 0.0, 0.5])
  ax3.set_xticklabels([-0.5, 0.0, 0.5])
  ax3.set_yticklabels([])

  # a-e plot
  gs2 = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=gs[1])

  ax4 = plt.subplot(gs2[1:,:])

  m, n = oh.mkline(mmin, 2.0, mmax, 25.0)
  s = df.mass * m + n

  ax4.scatter(df["a"], df["e"], color='k', marker='.', s=s**2., \
  	  edgecolor=None, zorder=5)

  ax4.set_xlim(0.0, rlim)
  ax4.set_ylim(0.0, 1.0)

  ax4.set_xlabel("Semi-Major Axis (AU)", fontsize=fs)
  ax4.set_ylabel("Eccentricity", fontsize=fs)

  # Mass Growth
  heavy  = df.sort_values(by=["mass"], ascending=False).head(5)
  masses = list(heavy["mass"])

  ax5 = plt.subplot(gs2[0,:])
  ax5.axis("off")

  ax5.text(0.6, 0.9, "Time (Myr)", fontsize=fs)
  t = float(df["time"].ix[0]) * 1.e-6
  ax5.text(0.6, 0.7, "{0:05.2f}".format(t), fontsize=fs)

  ax5.text(0.1, 0.9, "Top 5 Masses", fontsize=fs)
  for j, object in enumerate(masses):
	ax5.text(0.1, 0.70-(0.15*j), "{0:}.   {1:05.2f}".format(j+1, object), \
		fontsize=fs)

  # Thicker axes
  for axis in [ax1, ax2, ax3, ax4]:
	[j.set_linewidth(3) for j in axis.spines.itervalues()]
	axis.tick_params(axis='both', which='major', pad=10)
	axis.tick_params('both', length=5, width=2, which='major', labelsize=fs)
	axis.tick_params('both', length=3, width=1, which='minor')

  plt.savefig("frame{:>05}.png".format(i), \
  	  format='png', dpi=100)

  del df
  plt.clf()

#import os
#os.system("ffmpeg -r 12 -i frame%05d.png -vf scale=2048:1800 -qscale:v 0 -r 12 movie.avi")
#os.system("ffmpeg -r 30 -f image2 -s 640x480 -start_number 0 -i frame%05d.png \
#	-vframes 1000 -vcodec libx264 -crf 25  -pix_fmt yuv420p movie.mp4")

