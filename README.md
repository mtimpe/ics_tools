# ics tools

## Source KDE
![source kde](figures/quipu.png?raw=true "Source KDE")

## 1st-order collision statistics
![collision stats](figures/obliquity.png?raw=true "Collision Statistics")
