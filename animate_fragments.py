#!/usr/bin/env python

import numpy as np
from scipy import integrate

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import cnames
from matplotlib import animation

# Data types for reading Genga outputs
dtype = np.dtype([('t', 'float64'), ('pid', 'int'), \
        ('m', 'float64'), ('r', 'float64'),\
        ('x', 'float64'), ('y', 'float64'), ('z', 'float64'),\
        ('vx', 'float64'), ('vy', 'float64'), ('vz', 'float64')])

# Set up figure & 3D axis for animation
fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1], projection='3d')
#ax.axis('off')

# choose a different color for each trajectory
colors = plt.cm.jet(np.linspace(0, 1, 10))

# set up lines and points
lines = sum([ax.plot([], [], [], '-', c='red')
             for c in colors], [])
pts = sum([ax.plot([], [], [], '.', lw=4, c=c)
           for c in colors], [])

# prepare the axes limits
ax.set_xlim((-1, 1))
ax.set_ylim((-1, 1))
ax.set_zlim((-1, 1))

# set point-of-view: specified by (altitude degrees, azimuth degrees)
ax.view_init(45, 45)

# initialization function: plot the background of each frame
def init():
    for line, pt in zip(lines, pts):
        line.set_data([], [])
        line.set_3d_properties([])

        pt.set_data([], [])
        pt.set_3d_properties([])
    return lines + pts

# animation function

#xt = []
#yt = []
#zt = []

tails = {
        'x':{
            0:[],
            1:[],
            2:[],
            3:[],
            4:[],
            5:[],
            6:[],
            7:[],
            8:[],
            9:[],
            10:[]
            },
        'y':{
            0:[],
            1:[],
            2:[],
            3:[],
            4:[],
            5:[],
            6:[],
            7:[],
            8:[],
            9:[],
            10:[]
            },
        'z':{
            0:[],
            1:[],
            2:[],
            3:[],
            4:[],
            5:[],
            6:[],
            7:[],
            8:[],
            9:[],
            10:[]
            }
        }

def animate(i):
    # Load current step
    fn = 'Out_MERCURY_{:012d}.dat'.format(i)
    particles = np.loadtxt(fn, dtype=dtype, skiprows=0, usecols=(0,1,2,3,4,5,6,7,8,9))

    for line, pt, particle in zip(lines, pts, particles):
        pid = particle['pid']
        x, y, z = particle['x'], particle['y'], particle['z']

        tails['x'][pid].append(x)
        tails['y'][pid].append(y)
        tails['z'][pid].append(z)

        line.set_data(tails['x'][pid], tails['y'][pid])
        line.set_3d_properties(tails['z'][pid])

        pt.set_data(x, y)
        pt.set_3d_properties(z)


    pt.set_data([0], [0])
    pt.set_3d_properties([0])

    ax.view_init(45, (i*1)%360)
    fig.canvas.draw()
    return lines + pts

# instantiate the animator.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=10000, interval=30, blit=False)

# Save as mp4. This requires mplayer or ffmpeg to be installed
#anim.save('lorentz_attractor.mp4', fps=15, extra_args=['-vcodec', 'libx264'])

plt.show()
