#!/usr/bin/env python

from __future__ import print_function

'''
Once a collision has been detected by Genga, the impact parameters are passed to
this module. This module then sets up the necessary Gasoline input files and
submits a run to the SLURM queue.
'''


__author__ = "Miles Timpe"
__copyright__ = "Copyright 2016, ICS"
__license__ = "GPL"
__version__ = "0.0.0"
__maintainer__ = "Miles Timpe"
__credits__ = ["Simon Grimm", "Matthaeus Heer", "Christian Reinhardt"]
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"


import os
import sys
import argparse
import subprocess
import numpy as np
import pandas as pd
sys.path.insert(0, "/home/cluster/mtimpe/data/src/nsph/nsph")
import constants as c
import file_io as iof
import parameters as param
import profile as model
import perturbers
import softening

# Get command line arguments (passed from wrapper)
parser = argparse.ArgumentParser()
parser.add_argument('wdir', help="Working directory with collision files", type=str)
parser.add_argument('name', help="Name of run", type=str)
args = parser.parse_args()

# Collision files
f1 = '{}/Collisions_{}.dat'.format(args.wdir, args.name)
f2 = '{}/OutCollision.dat'.format(args.wdir)

# Get collision data from Genga
df = iof.getColliders(f1, f2)

df = df.sort_values(by='mass', ascending=0)

# Isolate bodies
body1 = df.iloc[0]
body2 = df.iloc[1]

# Isolate body 1 as target
pid1 = body1['pid']
m1 = body1['mass'] * c.GENGA2GAS_M
r1 = body1['radius'] * c.GENGA2GAS_L
p1 = np.array([body1['x'], body1['y'], body1['z']]) * c.GENGA2GAS_L
v1 = np.array([body1['vx'], body1['vy'], body1['vz']]) * c.GENGA2GAS_V

# Isolate body 2 as projectile
pid2 = body2['pid']
m2 = body2['mass'] * c.GENGA2GAS_M
r2 = body2['radius'] * c.GENGA2GAS_L
p2 = np.array([body2['x'], body2['y'], body2['z']]) * c.GENGA2GAS_L
v2 = np.array([body2['vx'], body2['vy'], body2['vz']]) * c.GENGA2GAS_V

# Calculate CoM
R_com = (m1*p1 + m2*p2) / (m1 + m2)
V_com = (m1*v1 + m2*v2) / (m1 + m2)

p1_com = p1 - R_com
p2_com = p2 - R_com

v1_com = v1 - V_com
v2_com = v2 - V_com


#=======================================================================#
# Impact parameter using closest point of approach method
r = p2 - p1
w = v2 - v1

# Output collision parameters
isep = np.linalg.norm(p2 - p1)  # initial separation (Gas. CU)
vrel = np.linalg.norm(v2 - v1)  # impact velocity (Gas. CU)

# Time until closest approach
if w.dot(w) < 0.000001:
	t_cpa = 0
else:
	t_cpa = -np.dot(r,w) / w.dot(w)

# Positions at closest approach
p1_cpa = p1 + (t_cpa * v1)
p2_cpa = p2 + (t_cpa * v2)
r_cpa = p2_cpa - p1_cpa

# Critical Radius
rcrit = r1 + r2  # (Gas. CU)

# Impact parameter
b = np.sqrt(r_cpa.dot(r_cpa)) / rcrit  # Dimensionless

# Mutual escape velocity
mass  = (m1 + m2) / c.GENGA2GAS_M  # Msun
rcrit = rcrit / c.GENGA2GAS_L  # AU
#vesc  = np.sqrt(2. * c.G_AU3 * mass / rcrit)  # AU/day
vesc  = np.sqrt(2. * mass / (rcrit * c.GENGA2GAS_L))  # Msun / km

# Impact velocity relative to mutual escape velocity
#vimp = (vrel / c.GENGA2GAS_V) / vesc
vimp = vrel / vesc

# Impact energy
mu = m1 * m2 / (m1 + m2)  # [Gas. CU]
QR = 0.5 * mu * w.dot(w) / (m1 + m2)  # [Gas. CU]
#QR_MJ = (QR * (e2j/sol2kg)) / 1.0e6  # megajoules per kg


#=======================================================================#

# Distribute total particles between bodies
n1 = int(param.gasoline_particles * m1 / (m1 + m2))
n2 = int(param.gasoline_particles * m2 / (m1 + m2))

# Surface temperatures
t1 = param.tsurface
t2 = param.tsurface

# Output collision parameters
print('\n\t  Body 1')
print('\t  m1 = {:.3f} Earth masses'.format(m1 * c.MSOL2ME / c.GENGA2GAS_M))
print('\t  r1 = {:.1f} km'.format(r1 * c.AU2KM / c.GENGA2GAS_L))
print('\t  v1 = {:.1f} km/s'.format(np.linalg.norm(v1) * c.GENGA2KMS / c.GENGA2GAS_V))
print('\t  n1 = {} particles'.format(n1))
print('\t  t1 = {:.1f} K'.format(t1))

print('\n\t  Body 2')
print('\t  m2 = {:.3f} Earth masses'.format(m1 * c.MSOL2ME / c.GENGA2GAS_M))
print('\t  r2 = {:.1f} km'.format(r2 * c.AU2KM / c.GENGA2GAS_L))
print('\t  v2 = {:.1f} km/s'.format(np.linalg.norm(v2) * c.GENGA2KMS / c.GENGA2GAS_V))
print('\t  n2 = {} particles'.format(n2))
print('\t  t2 = {:.1f} K'.format(t2))

print('\n\t  Critical Separation  = {:.1f} km'.format(rcrit * c.AU2KM))
print('\t  Impact Parameter (b) = {}'.format(b))
print('\t  Impact Velocity      = {:.6f} (km/s)'.format(vrel))
print('\t  Escape Velocity      = {:.6f} (km/s)'.format(vesc))
print('\t  Impact Velocity      = {:.1f} (vesc)'.format(vimp))
print('\t  Impact Energy        = {:.4f} (???)\n'.format(QR))


# If colliding particles identical, only build one model.

# Create target
print('\n[  SPH  ] Building target')
#print(m1, n1, param.tsurface, 'target')
model.makeProfile(m1, n1, t1, 'target')
print('\n\t  Target created!')

# Create projectile
print('\n[  SPH  ] Building projectile')
#print(m2, n2, param.tsurface, 'projectile')
model.makeProfile(m2, n2, t2, 'projectile')
print('\n\t  Projectile created!')


# Move bodies to pre-impact positions
foo = 'target.std {} {} {} {} {} {}'\
	.format(p1_com[0], p1_com[1], p1_com[2], v1_com[0], v1_com[1], v1_com[2])

bar = 'projectile.std {} {} {} {} {} {}'\
	.format(p2_com[0], p2_com[1], p2_com[2], v2_com[0], v2_com[1], v2_com[2])

print('\n[  SPH  ] Moving bodies to pre-impact positions\n')
foobar = '{}/collide_genga.single {} {} > collision.std'.format(param.lbin, foo, bar)

fout = open(os.devnull, 'w')
subprocess.call(foobar, shell=True, \
	stdout=fout, stderr=subprocess.STDOUT)
fout.close()

# Calculate softening parameter
f_target     = os.path.abspath('target.model')
f_projectile = os.path.abspath('projectile.model')
dsoft = softening.dsoft(f_target, f_projectile, n1, n2)

# Calculate dhmax
rho_min = 0.001
particle_mass = (m1 + m2) / param.gasoline_particles
dhmax = (3.0 * particle_mass / (4.0 * np.pi * rho_min))**(1.0/3.0)

# Update gasoline.par
with open('gasoline.par', 'r') as file:
  lines = file.readlines()
lines[0]  = 'dSoft = {}\n'.format(dsoft)
lines[6]  = 'achInFile = collision.std\n'
lines[7]  = 'achOutName = collision\n'
lines[33] = 'dhMax = {}\n'.format(dhmax)
lines[35] = 'nSteps = {}\n'.format(param.gasoline_steps)
lines[37] = 'iOutInterval = {}\n'.format(param.gasoline_interval)
with open('gasoline.par', 'w') as file:
  file.writelines( lines )

# Update gasoline.job
with open('gasoline.job', 'r') as file:
  lines = file.readlines()
lines[12] = 'FILE=collision\n'
lines[13] = 'STEPS={}\n'.format(param.gasoline_steps)
with open('gasoline.job', 'w') as file:
  file.writelines( lines )

