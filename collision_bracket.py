#!/usr/bin/env python

"""
Plot the collision history (i.e, a merger tree) for a given simulation.
"""
import sys
sys.path.insert(0, "/home/cluster/mtimpe/data/src/G3/Helpers")
import numpy as np
import matplotlib.pyplot as plt
import time
import glob
import argparse
import pandas as pd
import seaborn as sns
import io_helpers as ioh
import source_helpers as fh
import other_helpers as oh
from palettable.colorbrewer.qualitative import Paired_3
from matplotlib.collections import LineCollection

__author__ = "Miles Timpe"
__copyright__ = "Copyright 2016, Miles Timpe"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"

# Benchmark
tstart = time.time()

# ################################################################################
# Functions
# ################################################################################

def update_progress(progress):
	barLength = 10  # Modify this to change the length of the progress bar.
	status = ""
	if isinstance(progress, int):
		progress = float(progress)
	if not isinstance(progress, float):
		progress = 0
		status = "error: progress input must be a float\r\n"
	if progress < 0:
		progress = 0
		status = "Halt...\r\n"
	if progress >= 1:
		progress = 1
		status = "Done!\r\n"
	block = int(round(barLength*progress))
	text = "\r\t\t[{0}] {1:.0f}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
	sys.stdout.write(text)
	sys.stdout.flush()

def linewidths(dfp, scolor):
	# Scale Linewidth to Object Mass
	mmin = float(dfp["mass"].min())
	mmax = float(dfp["mass"].max())
	lmin = 0.1
	if mmin == mmax:
		lws = np.array(lmin * dfp["mass"])
	else:
		lmax = float(dfp["mass"].max() * m + n)
		slope = (lmax - lmin) / (mmax - mmin)
		offset = lmin - (slope * mmin)
		lws = np.array((slope * dfp["mass"]) + offset)
	# Linewidths Collection
	lwidths = lws[:-1]
	x = dfp["sma"]
	y = dfp["time"]
	points = np.array([x, y]).T.reshape(-1, 1, 2)
	segments = np.concatenate([points[:-1], points[1:]], axis=1)
	return LineCollection(segments, linewidths=lwidths, color=scolor)

def formationTime(df0, dfp, pid):
	# Calculate formation time for object with PID
	dff = df0[df0["pid"] == pid]
	mf = 0.99 * float(dff["mass"])
	dfForming = dfp[dfp["mass"] < mf]
	tf = dfForming["time"].max()
	dft = dfForming.tail(1)
	sma = np.sqrt(dft['x']*dft['x'] + dft['y']*dft['y'] + dft['z']*dft['z'])
	s = float(dft["mass"]) * m + n
	#return [float(dft.a), float(dft.time), s]
	return [float(sma), float(dft.time), s]

def semimajor(row):
	# Calculate semi-major axis of body from cartesian coords
	return np.sqrt(row['x']*row['x'] + row['y']*row['y'] + row['z']*row['z'])

# ################################################################################
# Parameters
# ################################################################################
# Debug Parameters
#debugStep = 100  # use only first N outputs
# Target Paremeters
ntop = 3  # highlight N most massive objects
# Figure Parameters
#figname = "merger_tree"
#figformat = "png"
#figdpi = 600
# Color Palette
cpalette_name = "colorbrewer.qualitative.Paired_3"
source_c = Paired_3.hex_colors
# y-axis Scale
# y-axis "Zero"
y0 = 1.1e6

# ################################################################################
# Parse/Glob
# ################################################################################
# Determine Run Name
runname = glob.glob("Out_*.dat")[0].split('_')[1]
figure_name = str(runname) + "_tree"
# Parse Arguments
parser = argparse.ArgumentParser()
parser.add_argument('--run_name', default=runname, \
		help='Name of Run')
parser.add_argument('--dir_name', default='.', \
		help='Directory')
parser.add_argument('--debug_mode', default='off', \
		help='Debug Mode on/off')
parser.add_argument('--unicode', default='on', \
		help='Enable unicode/progress bar')
parser.add_argument('--debug_step', default=int(100), \
		help='Debug Step')
parser.add_argument('--fig_name', default=figure_name, \
		help='Name of Output Figure')
parser.add_argument('--fig_format', default='png', \
		help='Figure Format (e.g., pdf, png')
parser.add_argument('--fig_dpi', default=int(600), \
		help='Figure Resolution')
parser.add_argument('--fig_scale', default='log', \
		help='Figure Scaling (linear or log)')
args = parser.parse_args()

debugStep = args.debug_step
yscale = args.fig_scale

# Glob Outputs
nsteps = []
globs = glob.glob("%s/Out*.dat" % args.dir_name)
globs = sorted(globs)
for g in globs:
	nstep = int(g.strip()[:-4].split("_")[-1])
	nsteps.append(nstep)

# Print Simulation Parameters
sys.stderr.write("\n\t# " + "#"*60 + "\n")
sys.stderr.write("\t# GHOST (Genga High mass Object Source Tree)\n")
sys.stderr.write("\t# " + "#"*60 + "\n")
sys.stderr.write("\n\t\tRun Name:\t{}\n".format(runname))

# Debug Mode
if args.unicode == "on":
	if args.debug_mode == "on":
		sys.stderr.write(u"\t\tDEBUG MODE\t[\u2713]\n")
	else:
		sys.stderr.write(u"\t\tDEBUG MODE\t[\u2717]\n")
else:
	if args.debug_mode == "on":
		sys.stderr.write(u"\t\tDEBUG MODE ACTIVE\n")
	else:
		sys.stderr.write(u"\t\tDEBUG MODE DISABLED\n")

# ################################################################################
# Load Simulation Files
# ################################################################################
print "\t// Loading Data"

# Load IC Output
fname = "Out_%s_%012d.dat" % (args.run_name, 0)
dfo_t0 = ioh.read_output_and_stack([fname], frame='heliocentric')
dfo_t0['sma'] = dfo_t0.apply(semimajor, axis=1)

# Pad IC Time for Log Plot
if args.fig_scale == "log":
	dfo_t0.time = dfo_t0.time + y0
mmin = dfo_t0["mass"].min()

if args.unicode == "on":
	sys.stderr.write(u'\t\tIC Output\t[\u2713]\n')
else:
	sys.stderr.write("\t\tIC Output\t[Loaded]\n")

# Load Final Output
lalala = 12100000000
fname = "Out_%s_%012d.dat" % (args.run_name, lalala)
dfo_tf = ioh.read_output_and_stack([fname], frame='heliocentric')
dfo_tf['sma'] = dfo_tf.apply(semimajor, axis=1)
tfinal = dfo_tf.iloc[0].time

mmax = dfo_tf["mass"].max()
if args.unicode == "on":
	sys.stderr.write(u'\t\tFinal Output\t[\u2713]\n')
else:
	sys.stderr.write("\t\tFinal Output\t[Loaded]\n")
# Load Collisions
fname = "Collisions_%s.dat" % args.run_name
dfc = ioh.read_collisions_and_stack([fname])

if args.unicode == "on":
	sys.stderr.write(u'\t\tCollisions\t[\u2713]\n')
else:
	sys.stderr.write("\t\tCollisions\t[Loaded]\n")

# ################################################################################
# Filter
# ################################################################################
# Select PID for Analysis
dfm = dfo_tf.sort_values(by=["mass"], ascending=False).head(ntop)

#targetPID = int(dfm.iloc[-1].pid)
#dfTarget = dfo_tf[dfo_tf["pid"] == targetPID]

dfTargets = pd.DataFrame()
sys.stderr.write("\t// Loading Targets\n".format(ntop))

# Populate Target Brackets
brackets_all = []
for i in range(1, ntop+1, 1):
	targetPID = int(dfm.iloc[-i].pid)
	dfTargets = dfTargets.append(dfo_tf[dfo_tf["pid"] == targetPID])

	print tfinal

	bracket = fh.return_bracket(targetPID, float(tfinal), dfc)
	brackets_all.append(bracket)
	
# Extract Final PIDs
fpids = np.asarray(dfTargets.pid, dtype=np.int64)
for i, pid in enumerate(fpids):
	sys.stderr.write("\t\tPID {}\t({}/{})\n".format(pid, i+1, len(fpids)))

# Loop Final PIDs
sys.stderr.write("\t// Building Source Trees\n")
sources_all = []
for idx, pid in enumerate(fpids):
	sys.stderr.write("\t\tPID {}\t({}/{})\n".format(pid, idx+1, len(fpids)))
	sources = fh.return_sources(pid, dfc)
	sources_all.append(sources)

# ################################################################################
# Loop Outputs
# ################################################################################
sys.stderr.write("\t// Reading Outputs\n")

# Debug Mode I/O
if args.debug_mode == "on":
	nsteps = nsteps[:args.debug_step]
else:
	nsteps = nsteps
"""
# Loop
df = pd.DataFrame()
for nstep in nsteps:
	# Progress Bar
	if args.unicode == "on":
		update_progress(float(nstep)/float(nsteps[-1]))
	else:
		sys.stderr.write("\t\t{} / {}\n".format(nstep, nsteps[-1]))
	# Load Output Files
	df1 = ioh.read_output("%s/Out_%s_%012d.dat" % (args.dir_name, args.run_name, nstep), frame="heliocentric")
	df1['sma'] = df1.apply(semimajor, axis=1)
	# Pad First Output if Log Plot
	if (args.fig_scale == "log") and (nstep == nsteps[0]):
		df1.time = df1.time + y0
	# Append Current Frame
	df = df.append(df1)
"""
# ################################################################################
# Setup Plot
# ################################################################################
# Set Scaling
m, n = oh.mkline(mmin, 5.0, mmax, 26.0)
# Marker Sizes
s0 = dfo_t0.mass * m + n
sf = dfo_tf.mass * m + n
# Setup Figure
sns.set(style="white", palette="muted", color_codes=True)
fig, ax = plt.subplots(1, 1, figsize=(9,9))
#sns.despine(left=True)
"""
# ICs
ax.scatter(dfo_t0["sma"], dfo_t0["time"], s=s0**2.0, c="black", edgecolor="none", zorder=99)
# Final
ax.scatter(dfo_tf["sma"], dfo_tf["time"], s=sf**2.0, c="black", edgecolor="none", zorder=99)

# Plot ALL Particle Paths
if args.debug_mode == "on":
	sys.stderr.write("\n\t// Background Trajectories Supressed for Debugging\n\n")
else:
	pids = list(set(df.pid))
	sys.stderr.write("\t// Plotting {} Particle Trajectories\n".format(len(pids)))
	for i, pid in enumerate(pids):
		# Progress Bar
		if args.unicode == "on":
			update_progress(float(i)/(float(len(pids)-1)))
		elif args.unicode == "off":
			sys.stderr.write("\t\t{} / {}\n".format(i, len(pids)-1))
		# Filter
		dfParticle = df[df.pid == pid]
		# Plot
		ax.plot(dfParticle["sma"], dfParticle["time"], color="LightGray", lw=1.0, alpha=1.0, zorder=0)
"""
for i, BRK in enumerate(brackets_all):
	for j, CLS in enumerate(BRK):
		print i,j,CLS

sys.exit()

# Plot Merger Tree for Target PIDs
sys.stderr.write("\t// Building Merger Trees\n")
for i, sources in enumerate(sources_all):
	pid = sources[0]
	sys.stderr.write("\t\tPID {}\n".format(pid))
	# Merger Tree Color
	pidc = source_c[i]
	# Filter
	dfPrimary = df[df.pid == sources[0]]
	# Formation Time (99% of Final Mass)
	f_arr = formationTime(dfo_tf, dfPrimary, pid)
	# Plot at Formation
	#ax.scatter(f_arr[0], f_arr[1], s=f_arr[2]**2.0, color="black", edgecolor="none", zorder=3)
	# Legend
	mtext = "{:.1f}".format(float(dfo_tf[dfo_tf["pid"] == sources[0]].mass))
	ttext = "{:.3f}".format(f_arr[1]/1.e6)
	xtxt = 3.
	if yscale == "log":
		ytxt = 1.e3 + 1.e6 * 10.**(-3 + (0.2*i))
		ax.text(xtxt, ytxt, mtext + r' M$_{\oplus}$,  ' + ttext + r' Myr', color=pidc)
	elif yscale == "linear":
		ytxt = 1.e6 * (i+1)
		ax.text(xtxt, ytxt, mtext + r' M$_{\oplus}$,  ' + ttext + r' Myr', color=pidc)
	# Before/After Formation Paths
	dfForming = dfPrimary[dfPrimary["time"] <= f_arr[1]]
	dfEvolving = dfPrimary[dfPrimary["time"] > f_arr[1]]
	#ax.plot(dfEvolving["sma"], dfEvolving["time"], color="black", lw=1, zorder=2)

	# Linewidths
	lc = linewidths(dfPrimary, pidc)  # old dfPrimary
	# Plot Target PIDs
	ax.add_collection(lc)

	# Loop Target PID Sources
	#print "\tPID {} Source Trajectories".format(pid)
	for j, source in enumerate(sources):
		# Progress Bar
		if args.unicode == "on":
			update_progress(float(j)/(float(len(sources)-1)))
		elif args.unicode == "off":
			sys.stderr.write("\t\t{} / {}\n".format(j, len(sources)-1))
		# Filter
		dfPID = df[df.pid == source]
		# Plot
		ax.plot(dfPID["sma"], dfPID["time"], color=pidc, lw=1.0, alpha=1.0, zorder=1)

	# Highlight Source Bodies
	dfSource = dfo_t0[dfo_t0["pid"].isin(sources)]
	s0 = dfSource.mass * m + n
	ax.scatter(dfSource["sma"], dfSource["time"], s=s0**2.0, color=pidc, edgecolor="none", zorder=2)

	# Highlight Final Body
	dfFinal = dfo_tf[dfo_tf["pid"] == int(sources[0])]
	sf = dfFinal.mass * m + n
	ax.scatter(dfFinal["sma"], dfFinal["time"], s=sf**2.0, color=pidc, edgecolor="none", zorder=2)

# ################################################################################
# Figure
# ################################################################################
# Show Parameters
sys.stderr.write("\t// Creating Figure\n")
sys.stderr.write("\t\tOutput Name:\t{}\n".format(args.fig_name))
sys.stderr.write("\t\tFormat:\t\t{}\t\n".format(args.fig_format))
sys.stderr.write("\t\tScale:\t\tlog-{}\t\n".format(args.fig_scale))
sys.stderr.write("\t\tResolution:\t{}\t\n".format(args.fig_dpi))
sys.stderr.write("\t\tPalette:\t{}\t\n".format(cpalette_name))

# Set Figure Properties
# x-axis
ax.set_xlabel(r'$\rm PID$')
# y-axis
ax.set_ylabel(r'$\log_{10}(t)$ $(Myr)$')
if yscale == "log":
	# Log
	ax.set_yscale('log')
	ax.set_ylim(3.e8, 1.e6)
	#ax.set_yticks([1.e3, 1.e4, 1.e5, 1.e6])
	#ax.set_yticklabels([0.001, 0.01, 0.1, 1, 10])
	#ax.set_yticklabels([-3, -2, -1, 0])
elif yscale == "linear":
	# Linear
	ax.set_ylim(1.1e7, -5e3)
	ax.set_yticks([1., 1.e6, 2.e6, 3.e6, 4.e6, 5.e6, 6.e6, 7.e6, 8.e6, 9.e6, 1.e7])
	ax.set_yticklabels([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
else:
	sys.exit("error: y-axis scale must be linear or log")

# Legend Title
ylegend = 1.e3 + 1.e6 * 10.**(-3.3)
ax.text(3, ylegend, "Objects", color="Black")
# Figure Title
ax.set_title(args.run_name)

plt.savefig("/home/cluster/mtimpe/figures/" + args.fig_name + "." + args.fig_format, \
		format=args.fig_format, dpi=args.fig_dpi)

print "\t// Done\n"

# Report Benchmark
print "\n\t{:.2f} seconds\n".format(time.time() - tstart)
