#!/usr/bin/env python


import argparse
import glob
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.gridspec as gridspec
#from matplotlib.font_manager import FontProperties
import matplotlib.font_manager
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, "/home/mtimpe/Projects/ics_tools/Helpers")
import io_helpers as io
import source_helpers as sh
import kepler_helpers as kh
import constants as c


# Arguments_____________________________________________________________________

parser = argparse.ArgumentParser()

parser.add_argument('--style', default='light', \
                    help='figure style.')

parser.add_argument('--yaxis', default='pid_tree', \
                    help='yaxis quantity')

parser.add_argument('--no_leaves', default=True, action='store_false')

args = parser.parse_args()


# Functions_____________________________________________________________________

def impact_energy(row):
    # Leinhardt & Stewart (2012)

    # Reduced mass
    mu = row['mi'] * row['mj'] / (row['mi'] + row['mj'])  # g
    # Total mass
    mtotal = row['mi'] + row['mj']
    # Impact velocity
    vimp = float(row['v_impact']) * 1000.0  # m/s
    # Specific impact energy
    return 0.5 * mu * vimp**2.0 / (mtotal * 1.e6)  # Megajoules


def semi_major_axis(row):
    """
    @params
    r - (x,y,z) Cartesian Positions
    v - (vx,vy,vz) Cartesian Velocities
    mass - Particle Mass
    central_mass - Mass of Central Object

    @returns
    a - Semi-Major Axis

    Cf. http://www.bruce-shapiro.com/pair/ElementConversionRecipes.pdf
    """

    # Unpack row
    m1 = float(row['mi']) * MEARTH / MSUN
    p1 = np.array([row['xi'], row['yi'], row['zi']], dtype=float) #/ c.genga_to_kms
    v1 = np.array([row['vxi'], row['vyi'], row['vzi']], dtype=float) / c.genga_to_kms

    m2 = float(row['mj']) * MEARTH / MSUN
    p2 = np.array([row['xj'], row['yj'], row['zj']], dtype=float) #/ c.genga_to_kms
    v2 = np.array([row['vxj'], row['vyj'], row['vzj']], dtype=float) / c.genga_to_kms

    # Gravitational Parameter
    central_mass=1.0
    G = 1.0

    # Cartesian Unit Vectors
    iHat = np.array([1., 0., 0.])
    jHat = np.array([0., 1., 0.])
    kHat = np.array([0., 0., 1.])

    semimajor = []

    for mass, r, v in zip([m1,m2], [p1,p2], [v1,v2]):

        mu = G * ( central_mass + mass )

        # Eccentricity
        h = np.cross(r, v)
        evec = 1. / mu * np.cross(v, h) - r / np.linalg.norm(r)
        ecc = np.linalg.norm(evec)

        # Semi Major Axis
        a = np.dot(h,h) / ( mu * ( 1. - ecc**2. ))

        semimajor.append(a)

    return pd.Series({'ai':semimajor[0], 'aj':semimajor[1]})


def mkline(x1, y1, x2, y2):
    m = (y2 - y1) / (x2 - x1)
    n = y1 - m * x1
    return m, n


# Parameters____________________________________________________________________

# xaxis
xaxis_type = 'time'
yaxis_type = args.yaxis  #'pid_tree'  # pid_timeline, 'semimajor'
scale_type = 'SPE'


# Constants_____________________________________________________________________
# Solar System Bodies (NASA JPL)
# http://ssd.jpl.nasa.gov/?planet_phys_par
# Sun
MSUN   = 1.9889e+33  # mass of Sun    [g]
# Earth
MEARTH = 5.974e+27   # mass of Earth  [g]


# Main__________________________________________________________________________

# Collision file
cf1 = '/home/mtimpe/Projects/ics_tools/data/Gstar/Collisions_R001.dat'
cf2 = '/home/mtimpe/Projects/ics_tools/data/Gstar/Collisions_R002.dat'
cf3 = '/home/mtimpe/Projects/ics_tools/data/Gstar/Collisions_R003.dat'
cf4 = '/home/mtimpe/Projects/ics_tools/data/Gstar/Collisions_R004.dat'
cf5 = '/home/mtimpe/Projects/ics_tools/data/Gstar/Collisions_R005.dat'

#cf = [cf1,cf2,cf3,cf4,cf5]

cf = glob.glob('/home/mtimpe/Projects/ics_tools/data/A0M060N2noG/r*/*dat')

cf = cf[:5]

#cf1 = '/home/mtimpe/Projects/ics_tools/data/Collisions_1P_M15_N10K_NG.dat'

# Setup figure__________________________________________________________________

fig1 = plt.figure(figsize=(34,20), facecolor='white')

gs = gridspec.GridSpec(2, 1,height_ratios=[1,5])

if args.style == 'dark':

    plt.style.use('dark_background')
    #fig1, ax1 = plt.subplots(2, 1, figsize=(30,20))

    lc = 'snow'  # line color
    hc = 'DeepSkyBlue'  # highlight line color
    tc = 'snow'  # text color

elif args.style == 'light':

    #fig1, ax1 = plt.subplots(2, 1, figsize=(30,20), facecolor='white')

    lc = 'dimgray'  # line color
    hc = 'DeepSkyBlue'  # highlight line color
    tc = 'black'  # text color


smin = -1
smax = 5

# Set Scaling
sm, sn = mkline(smin, 1.0, smax, 50.0)

cpalette = cm = plt.get_cmap('autumn_r')
cNorm  = colors.Normalize(vmin=smin, vmax=smax)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cpalette)

lw = 2   # line width
alpha=1  # transparency

# Panel
#ax = ax1[1]
ax = plt.subplot(gs[1])

# X-axis
#print('\nFirst collision: {:.1f} yrs\n'.format(np.min(df.time)))
#xmin = 1e4
#xmax = 1e7
#xmin = 1e5
#xmax = 2e8
xmin = 1e4
xmax = 1e7
ax.set_xlabel(r'$\rm Time$ $\rm (Myr)$', fontsize=22)
ax.set_xscale('log')
#ax.set_xlim(0,1e7)
ax.set_xlim(xmin,xmax)
#ax.set_xticks([1e5,1e6,1e7,1e8,2e8])
#ax.set_xticklabels([0.1,1,10,100,200], fontsize=16)
ax.set_xticks([1e4,1e5,1e6,1e7])
ax.set_xticklabels([0.01,0.1,1,10], fontsize=16)

#ax.set_xticks([0, 0.5e8, 1e8, 1.5e8, 2e8])
#ax.set_xticklabels([0, 50, 100, 150, 200], fontsize=16)

# Y-axis
if args.yaxis in ['pid_tree', 'pid_timeline']:

    if args.no_leaves:

        ymin = 100
        ymax = 0
        ax.set_ylabel('PID')
        ax.set_ylim(ymin+1,ymax-1)

    else:

        ymin = -1
        ymax = len(cf)
        #ax.set_ylabel('Run')
        ax.set_ylim(ymin,ymax)

        ax.set_yticks([])
        ax.set_yticklabels([])

    #ax.set_yticks(list(set(source_tree)))
    #ax.set_yticklabels(list(set(source_tree)), fontsize=8, color='k')

    ax.spines['left'].set_visible(False)

elif args.yaxis == 'semimajor':

    ax.set_ylabel('$a$ $(AU)$')
    ax.set_ylim(0,5)
    #print('Maximum semi-major axis: {:.3f}'.format(np.max(df.ai)))


ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
#ax.spines['bottom'].set_visible(False)

ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')


# File I/O______________________________________________________________________

all_sources = []
tcollisions = []
tgiantimpacts = []
globalSPEmin = []
globalSPEmax = []

for cdx, cfile in enumerate(cf):

    print(cfile)

    df = io.read_collisions_and_stack([cfile],
            return_xyz=True, return_geometry=True)

    df['SPE'] = df.apply(impact_energy, axis=1)

    df = df.merge(df.apply(semi_major_axis, axis=1),
            left_index=True, right_index=True)

    # Find most massive object
    if np.max(df['mi']) > np.max(df['mj']):
        dff = df[df['mi'] == np.max(df['mi'])]

        if len(dff) > 1:
            dff = dff.iloc[0]

        pid = int(dff['pidi'])
    else:
        dff = df[df['mj'] == np.max(df['mj'])]

        if len(dff) > 1:
            dff = dff.iloc[0]

        pid = int(dff['pidj'])


    df.sort_values(by='time')

    #df = df.loc[(df['pidi'] == pid) | (df['pidj'] == pid)]

    if np.max(df['mi']) > np.max(df['mj']):
        target = int(df.iloc[[df['mi'].idxmax()]]['pidi'])
        final_mass = float(df.iloc[[df['mi'].idxmax()]]['mi'])
    else:
        target = int(df.iloc[[df['mj'].idxmax()]]['pidj'])
        final_mass = float(df.iloc[[df['mj'].idxmax()]]['mj'])

    source_tree = sh.return_sources(target, df)

    all_sources.extend(source_tree)


    df = df.loc[(df['pidi'].isin(source_tree)) | (df['pidj'].isin(source_tree))]

    #print(df)

    # Main__________________________________________________________________________

    # Keep track of last node for PIDs
    pids = {}

    ncollisions = 0

    # Iterate over nodes (collisions)
    i = 0
    for idx, row in df.iterrows():
        i += 1

        x = float(row['time'])

        pidi = int(row['pidi'])
        pidj = int(row['pidj'])


        # Which particle is more massive?
        if row['mi'] > row['mj']:
            pidm = int(row['pidi'])
            #a = row['ai']
        else:
            pidm = int(row['pidj'])
            #a = row['aj']


        # If either particle already involved in a collision
        if pidi in pids:
            xi = pids[pidi]['x']
            yi = pids[pidi]['y']
            ai = pids[pidi]['a']
        else:
            xi = 0
            yi = pidi
            ai = float(row['ai'])

        if pidj in pids:
            xj = pids[pidj]['x']
            yj = pids[pidj]['y']
            aj = pids[pidj]['a']
        else:
            xj = 0
            yj = pidj
            aj = float(row['aj'])


        # Determine y-value
        if args.yaxis == 'pid_tree':
            y = (yi + yj) / 2.0

        elif args.yaxis == 'pid_timeline':
            y = pidm

        elif args.yaxis == 'semimajor':
            y = (ai + aj) / 2.0



        # Record new node
        if row['mi'] > row['mj']:
            pids[pidi] = {'x':x, 'y':y, 'a':y}
        else:
            pids[pidj] = {'x':x, 'y':y, 'a':y}

        # Marker Sizes
        #s = (row['SPE'] * sm + sn)**2.

        s = (np.log10(row['SPE']) * sm + sn)**2.

        #if s > 300:
        #    s = 300


        globalSPEmin.append(np.min(np.log10(row['SPE'])))
        globalSPEmax.append(np.max(np.log10(row['SPE'])))


        # Scale node size to specific impact energy
        #s = (3.0 * row['SPE'])**2.0

        # Plot

        # Nodes
        nc = scalarMap.to_rgba(np.log10(row['SPE']))

        if args.yaxis == 'semimajor':
            yi = ai
            yj = aj

        if args.no_leaves:

            ax.plot([x,x], [yi,yj], color=lc, lw=lw, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.plot([xi,x], [yi,yi], color=lc, lw=lw, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.plot([xj,x], [yj,yj], color=lc, lw=lw, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.scatter([x], [y], color=nc, zorder=9,
                    marker='o', s=s, alpha=0.8)

        else:
            y = cdx
            yi = cdx
            yj = cdx

        if pidi == target:
            ax.plot([x,x], [yi,y], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.plot([xi,x], [yi,yi], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.scatter([x], [y], color=nc, zorder=9,
                    marker='o', s=s, alpha=0.8)
            ncollisions += 1

            tcollisions.append(row['time'])
            if float(row['SPE']) > 0.3:
                tgiantimpacts.append(row['time'])

        elif pidj == target:
            ax.plot([x,x], [yj,y], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.plot([xj,x], [yj,yj], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                    solid_capstyle='round')
            ax.scatter([x], [y], color=nc, zorder=9,
                    marker='o', s=s, alpha=0.8)
            ncollisions += 1

            tcollisions.append(row['time'])
            if float(row['SPE']) > 0.3:
                tgiantimpacts.append(row['time'])


        if i == len(df):
            simend = 1e7  #2.e8

            padx = (xmax - xmin) * 0.01

            ax.plot([x, simend], [y,y], color=lc, lw=lw, zorder=1,
                    solid_capstyle='round')

            #ax.text(xmin-1e3, y, '{:04d}'.format(pidm),
            #        color=tc, ha='right', va='center',
            #        fontname='DejaVu Sans')

            #ax.text(simend+1e6, y, 'N={}'.format(ncollisions),
            #        color=tc, ha='left', va='center',
            #        fontname='DejaVu Sans')

            ax.text(xmin-1e3, y, '{}'.format(ncollisions),
                    color=tc, ha='right', va='center',
                    fontname='DejaVu Sans')

            ax.text(simend+1e6, y, r'{:.1f} $\rm M_{{\oplus}}$'.format(final_mass),
                    color=tc, ha='left', va='center',
                    fontname='DejaVu Sans')


#qcrit_size = (np.log10(0.3) * sm + sn)**2.

#qcrit_color = scalarMap.to_rgba(np.log10(0.3))

#qcrit_x = 0.01 * 1e6

#ax.scatter([qcrit_x], [ymax-.01], color=qcrit_color, zorder=9,
#        marker='o', s=qcrit_size, alpha=0.8)

#ax.text(qcrit_x+1e2, ymax-.01, r'$\rm Q_{crit} = 0.3 \times 10^{6}$ $\rm J/kg$',
#        color=tc, ha='left', va='center', fontsize=22,
#        fontname='DejaVu Sans')


#with open('timeseries.dat', 'w') as f:
#    for t in tcollisions:
#        f.write('{}\n'.format(t))


# Y-axis
if args.yaxis in ['pid_tree', 'pid_timeline']:

    if args.no_leaves:

        ax.set_yticks(list(set(all_sources)))
        ax.set_yticklabels(list(set(all_sources)), fontsize=8, color='k')

for axis in ['bottom']:
  ax.spines[axis].set_linewidth(2)
  ax.xaxis.set_tick_params(width=2)


globalSPEmin = [x for x in globalSPEmin if str(x) != 'nan']
globalSPEmax = [x for x in globalSPEmax if str(x) != 'nan']

print('\nSPE min: {}\nSPE max: {}\n'.format(np.min(globalSPEmin), np.max(globalSPEmax)))


#ax = ax1[0]
ax = plt.subplot(gs[0])

ax.set_xscale('log')
ax.axis('off')

ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.spines['right'].set_visible(False)

#ax.set_xlabel(r'$\rm Time$ $(Myr)$')

bins = 10**np.linspace(np.log10(xmin), np.log10(xmax), num=61)

ax.hist(tcollisions, bins=bins, edgecolor='DeepSkyBlue', facecolor='DeepSkyBlue', alpha=1, label='All Collisions')

ax.hist(tgiantimpacts, bins=bins, edgecolor='#cd6155', facecolor='#cd6155', alpha=1, label='Giant Impacts')

leg = ax.legend(loc=1, frameon=False, prop={'size':18})

#plt.legend(frameon=False)

plt.subplots_adjust(hspace=None)

plt.savefig('/home/mtimpe/Desktop/dendrogram.svg', format='svg')

#print(globalSPEmin)
#print(globalSPEmax)

#globalSPEmin = globalSPEmin[~np.isnan(np.array(globalSPEmin))]
#globalSPEmax = globalSPEmax[~np.isnan(np.array(globalSPEmax))]

plt.show()
