#!/usr/bin/env python


import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
#from matplotlib.font_manager import FontProperties
import matplotlib.font_manager
import numpy as np
import pandas as pd
import sys
sys.path.insert(0, "/home/mtimpe/Projects/ics_tools/Helpers")
import io_helpers as io
import source_helpers as sh
import kepler_helpers as kh
import constants as c


# Arguments_____________________________________________________________________

parser = argparse.ArgumentParser()

parser.add_argument('--style', default='light', \
                    help='figure style.')

parser.add_argument('--yaxis', default='pid_tree', \
                    help='yaxis quantity')

args = parser.parse_args()


# Functions_____________________________________________________________________

def impact_energy(row):
    # Leinhardt & Stewart (2012)

    # Reduced mass
    mu = row['mi'] * row['mj'] / (row['mi'] + row['mj'])  # g
    # Total mass
    mtotal = row['mi'] + row['mj']
    # Impact velocity
    vimp = float(row['v_impact']) * 1000.0  # m/s
    # Specific impact energy
    return 0.5 * mu * vimp**2.0 / (mtotal * 1.e6)  # Megajoules


def semi_major_axis(row):
    """
    @params
    r - (x,y,z) Cartesian Positions
    v - (vx,vy,vz) Cartesian Velocities
    mass - Particle Mass
    central_mass - Mass of Central Object

    @returns
    a - Semi-Major Axis

    Cf. http://www.bruce-shapiro.com/pair/ElementConversionRecipes.pdf
    """

    # Unpack row
    m1 = float(row['mi']) * MEARTH / MSUN
    p1 = np.array([row['xi'], row['yi'], row['zi']], dtype=float) #/ c.genga_to_kms
    v1 = np.array([row['vxi'], row['vyi'], row['vzi']], dtype=float) / c.genga_to_kms

    m2 = float(row['mj']) * MEARTH / MSUN
    p2 = np.array([row['xj'], row['yj'], row['zj']], dtype=float) #/ c.genga_to_kms
    v2 = np.array([row['vxj'], row['vyj'], row['vzj']], dtype=float) / c.genga_to_kms

    # Gravitational Parameter
    central_mass=1.0
    G = 1.0

    # Cartesian Unit Vectors
    iHat = np.array([1., 0., 0.])
    jHat = np.array([0., 1., 0.])
    kHat = np.array([0., 0., 1.])

    semimajor = []

    for mass, r, v in zip([m1,m2], [p1,p2], [v1,v2]):

        mu = G * ( central_mass + mass )

        # Eccentricity
        h = np.cross(r, v)
        evec = 1. / mu * np.cross(v, h) - r / np.linalg.norm(r)
        ecc = np.linalg.norm(evec)

        # Semi Major Axis
        a = np.dot(h,h) / ( mu * ( 1. - ecc**2. ))

        semimajor.append(a)

    return pd.Series({'ai':semimajor[0], 'aj':semimajor[1]})


# Parameters____________________________________________________________________

# xaxis
xaxis_type = 'time'
yaxis_type = args.yaxis  #'pid_tree'  # pid_timeline, 'semimajor'
scale_type = 'spe'

# Font
#font1 = FontProperties()
#font1.set_size('large')
#font1.set_family('sans-serif')
#font1.set_weight('medium')


# Constants_____________________________________________________________________
# Solar System Bodies (NASA JPL)
# http://ssd.jpl.nasa.gov/?planet_phys_par
# Sun
MSUN   = 1.9889e+33  # mass of Sun    [g]
# Earth
MEARTH = 5.974e+27   # mass of Earth  [g]


# Main__________________________________________________________________________

# Collision file
cf1 = '/home/mtimpe/Projects/ics_tools/data/Gstar/Collisions_R002.dat'
#cf1 = '/home/mtimpe/Projects/ics_tools/data/Collisions_1P_M15_N10K_NG.dat'

df = io.read_collisions_and_stack([cf1],
        return_xyz=True, return_geometry=True)

df['SPE'] = df.apply(impact_energy, axis=1)

df = df.merge(df.apply(semi_major_axis, axis=1),
        left_index=True, right_index=True)

# Find most massive object
if np.max(df['mi']) > np.max(df['mj']):
    dff = df[df['mi'] == np.max(df['mi'])]
    pid = int(dff['pidi'])
else:
    dff = df[df['mj'] == np.max(df['mj'])]
    pid = int(dff['pidj'])


df.sort_values(by='time')

#df = df.loc[(df['pidi'] == pid) | (df['pidj'] == pid)]

if np.max(df['mi']) > np.max(df['mj']):
    target = int(df.iloc[[df['mi'].idxmax()]]['pidi'])
else:
    target = int(df.iloc[[df['mj'].idxmax()]]['pidj'])

source_tree = sh.return_sources(target, df)


df = df.loc[(df['pidi'].isin(source_tree)) | (df['pidj'].isin(source_tree))]

print(df)

# Setup figure__________________________________________________________________

if args.style == 'dark':

    plt.style.use('dark_background')
    fig1, ax1 = plt.subplots(1, 1, figsize=(30,20))

    lc = 'snow'  # line color
    hc = 'DeepSkyBlue'  # highlight line color
    tc = 'snow'  # text color

elif args.style == 'light':

    fig1, ax1 = plt.subplots(1, 1, figsize=(30,20), facecolor='white')

    lc = 'dimgray'  # line color
    hc = 'DeepSkyBlue'  # highlight line color
    tc = 'black'  # text color


cpalette = cm = plt.get_cmap('autumn_r')
cNorm  = colors.Normalize(vmin=0, vmax=10)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cpalette)

lw = 2   # line width
alpha=1  # transparency

# Panel
ax = ax1

# X-axis
print('\nFirst collision: {:.1f} yrs\n'.format(np.min(df.time)))
xmin = 5e4
xmax = 2e8
#xmin = 1e0
#xmax = 1e4
ax.set_xlabel(r'$\rm Time$ $\rm (Myr)$', fontsize=22)
ax.set_xscale('log')
#ax.set_xlim(1e5,3e8)
ax.set_xlim(xmin,xmax)
ax.set_xticks([1e5,1e6,1e7,1e8,2e8])
ax.set_xticklabels([0.1,1,10,100,200], fontsize=16)

# Y-axis
if args.yaxis in ['pid_tree', 'pid_timeline']:

    ymin = 100
    ymax = 0
    ax.set_ylabel('PID')
    ax.set_ylim(ymin+1,ymax-1)

    #ax.set_yticks(np.arange(ymax,ymin+1))
    #ax.set_yticklabels(np.arange(ymax,ymin+1), fontsize=8)

    ax.set_yticks(list(set(source_tree)))
    ax.set_yticklabels(list(set(source_tree)), fontsize=8, color='k')

    ax.spines['left'].set_visible(False)

elif args.yaxis == 'semimajor':

    ax.set_ylabel('$a$ $(AU)$')
    ax.set_ylim(0,5)
    print('Maximum semi-major axis: {:.3f}'.format(np.max(df.ai)))


ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
#ax.spines['bottom'].set_visible(False)

ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')


# Main__________________________________________________________________________

# Keep track of last node for PIDs
pids = {}

# Iterate over nodes (collisions)
i = 0
for idx, row in df.iterrows():
    i += 1

    x = float(row['time'])

    pidi = int(row['pidi'])
    pidj = int(row['pidj'])


    # Which particle is more massive?
    if row['mi'] > row['mj']:
        pidm = int(row['pidi'])
        #a = row['ai']
    else:
        pidm = int(row['pidj'])
        #a = row['aj']


    # If either particle already involved in a collision
    if pidi in pids:
        xi = pids[pidi]['x']
        yi = pids[pidi]['y']
        ai = pids[pidi]['a']
    else:
        xi = 0
        yi = pidi
        ai = float(row['ai'])

    if pidj in pids:
        xj = pids[pidj]['x']
        yj = pids[pidj]['y']
        aj = pids[pidj]['a']
    else:
        xj = 0
        yj = pidj
        aj = float(row['aj'])


    # Determine y-value
    if args.yaxis == 'pid_tree':
        y = (yi + yj) / 2.0

    elif args.yaxis == 'pid_timeline':
        y = pidm

    elif args.yaxis == 'semimajor':
        y = (ai + aj) / 2.0



    # Record new node
    if row['mi'] > row['mj']:
        pids[pidi] = {'x':x, 'y':y, 'a':y}
    else:
        pids[pidj] = {'x':x, 'y':y, 'a':y}


    # Scale node size to specific impact energy
    s = (3.0 * row['SPE'])**2.0

    # Plot

    # Nodes
    nc = scalarMap.to_rgba(row['SPE'])

    if args.yaxis == 'semimajor':
        yi = ai
        yj = aj

    ax.plot([x,x], [yi,yj], color=lc, lw=lw, zorder=1, alpha=alpha,
            solid_capstyle='round')
    ax.plot([xi,x], [yi,yi], color=lc, lw=lw, zorder=1, alpha=alpha,
            solid_capstyle='round')
    ax.plot([xj,x], [yj,yj], color=lc, lw=lw, zorder=1, alpha=alpha,
            solid_capstyle='round')
    ax.scatter([x], [y], color=nc, zorder=9,
            marker='o', s=s, alpha=0.8)


    if pidi == target:
        ax.plot([x,x], [yi,y], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                solid_capstyle='round')
        ax.plot([xi,x], [yi,yi], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                solid_capstyle='round')

    elif pidj == target:
        ax.plot([x,x], [yj,y], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                solid_capstyle='round')
        ax.plot([xj,x], [yj,yj], color=hc, lw=lw+1, zorder=1, alpha=alpha,
                solid_capstyle='round')


    if i == len(df):
        padx = (xmax - xmin) * 0.1

        ax.plot([x,np.max(df.time)+padx], [y,y], color=lc, lw=lw, zorder=1,
                solid_capstyle='round')

        ax.text(np.max(df.time)+padx, y, 'PID {:04d}'.format(pidm),
                color=tc, ha='left', va='center',
                fontname='DejaVu Sans')
                #fontproperties=font1)

    # Need previous nodes for pidi and pidj

plt.savefig('/home/mtimpe/Desktop/dendrogram.png', format='png')
plt.show()
