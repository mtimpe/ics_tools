#!/usr/bin/env python

import matplotlib.pyplot as plt
import pandas as pd

f = "skid_vs_time.dat"

df = pd.read_csv(f, delim_whitespace=True, header=None)

df.columns = ["step", "nDark", "nGas", "nStar", "unbound"]

fig, ax = plt.subplots(1, 1, facecolor="white")

ax.set_xlabel("Step")
ax.set_ylabel("N")

ax.plot(df["step"], df["unbound"], '-r', lw=3)

plt.show()
