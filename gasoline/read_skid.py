#!/usr/bin/env python

from __future__ import print_function

"""Reads SKID .stat file

PEP 257 is followed. Ideally, the authorship information below should be in a
separate LICENSE file unless the code is a standalone.
"""

__author__ = "Miles Timpe"
__copyright__ = "Copyright 2017, Miles Timpe"
__credits__ = ["Miles Timpe"]
__license__ = "The MIT License"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"

import glob
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np
import sys

# What are we analysing?
run = "/home/cluster/mtimpe/data/projects/collisions/rotation/M040P10K/output"


# Functions
def column(matrix, i):
    return [row[i] for row in matrix]


# No Pandas
stats = glob.glob("{}/*.stat".format(run))
stats = sorted(stats, key=lambda name: int(name[-10:-5]))
#nstat = len(stats)


# Values to track in next loop
steps = []
groups = []
pts = []

# Iterate through ordered .stat files
for i, stat in enumerate(stats):

    step = int(stat[-10:-5])

    with open(stat, 'r') as f:
        for j, line in enumerate(f):
            row = line.split()

            group   = int(row[0])     # group designation
            members = int(row[1])     # number of particles in group
            totmass = float(row[2])   # mass of group in code units
            xCenter = float(row[16])
            yCenter = float(row[17])
            zCenter = float(row[18])
            xVcm    = float(row[19])
            yVcm    = float(row[20])
            zVcm    = float(row[21])

    print("{0:05d}\tGroups: {1}".format(step, j+1))

    steps.append(step)
    groups.append(j+1)
    pts.append([step,group,totmass,xCenter,yCenter,zCenter])


s = column(pts,0)
g = column(pts,1)
m = column(pts,2)
x = column(pts,3)
y = column(pts,4)
z = column(pts,5)

grps = list(set(g))

pts = np.asarray(pts)

print(pts)

sys.exit()

# Group spatial
fig = plt.figure()
axis = fig.add_axes([0,0,1,1], projection="3d")
axis.axis("off")

lmin = min([min(x),min(y),min(z)])
lmax = max([max(x),max(y),max(z)])

axis.set_xlim(lmin, lmax)
axis.set_ylim(lmin, lmax)
axis.set_zlim(lmin, lmax)

# Choose a different color for each group
colors = plt.cm.jet(np.linspace(0, 1, len(grps)))

# Set up lines and points
lines = sum([axis.plot([],[],[], '-', c=c) for c in grps], [])
points = sum([axis.plot([],[],[], 'o', c=c) for c in grps], [])

# Initialize viewpoint
axis.view_init(30,0)

#axis.set_xlabel("xCenter")
#axis.set_ylabel("yCenter")
#axis.set_zlabel("zCenter")


def init():
    for line, pt in zip(lines, points):
        line.set_data([],[])
        line.set_3d_properties([])

        pt.set_data([],[])
        pt.set_3d_properties([])
    return lines + points


def animate(i):

    for line, pt, p in zip(lines, points, pts):
        u = p[:i]
        x, y, z = u[3], u[4], u[5]
        line.set_data(x,y)
        line.set_3d_properties(z)

        pt.set_data(x[-1:], y[-1:])
        pt.set_3d_properties(z[-1:])

    axis.view_init(30,0)
    fig.canvas.draw()
    return lines + points


anim = animation.FuncAnimation(fig, animate, init_func=init, frames=5000, interval=1, blit=True)

plt.show()


#mmin, mmax = min(m), max(m)

# Plot
#fig1, ax1 = plt.subplots(2, 1, facecolor="white")

# Number of groups
#axis = ax1[0]
#axis.set_xlabel("Step")
#axis.set_xlim(min(steps), max(steps))
#axis.set_ylabel(r"$N_{groups}$")
#axis.plot(steps, groups, "r-")
