#!/usr/bin/env python

from __future__ import print_function

"""Determines resources expended during Gasoline run.

PEP 257 is followed. Ideally, the authorship information below should be in a
separate LICENSE file unless the code is a standalone.
"""

__author__ = "Miles Timpe"
__copyright__ = "Copyright 2017, Miles Timpe"
__credits__ = ["Miles Timpe"]
__license__ = "The MIT License"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Development"

import glob
import numpy as np
import subprocess
import sys


# What are we analysing?
run = "/home/cluster/mtimpe/data/projects/collisions/rotation/M040P10K/output"

# File names for this run
skid = "/home/cluster/mtimpe/bin/skid-hop.single"
param = "gasoline.par"

# Functions

# Constants [cgs]
# https://www.cfa.harvard.edu/~dfabricant/huchra/ay145/constants.html
msolar = 1.989e+33  # [g] (solar mass)
kpc  = 3.085678e+21  # [cm] (kiloparsec)
ngc = 6.67259e-08  # [cm3 g-1 s-2] (newton's gravitational constant)
jd2s = 86400.0  # [s] (julian day)

# Greeting
print("\nAve, Imperator!")
print("\nThe following Gasoline run will be analysed:\n\n\t{}".format(run))


# How many output steps?
outputs = glob.glob("{}/*.den".format(run))
outputs = sorted(outputs, key=lambda name: int(name[-9:-4]))
nout = len(outputs)
print("\nThere are {} outputs in this run.".format(nout))


# Read parameter file and determine timestep

with open("{}/{}".format(run,param), 'r') as f:
    for line in f:
        if line.startswith("#"):
            continue
        line = line.rstrip().split("=")
        par = line[0].strip()
        val = line[1].strip()
        
        if par == "achOutName":
            name = str(val)
        elif par == "dKpcUnit":
            dKpcUnit = float(val)
        elif par == "dMsolUnit":
            dMsolUnit = float(val)
        elif par == "dDelta":
            delta = float(val)
        elif par == "nSteps":
            steps = int(val)

# Gasoline timestep
internal_dt = np.sqrt((dKpcUnit * kpc)**3. / (ngc * dMsolUnit * msolar))  # seconds

dt = delta * internal_dt

ttot = (steps * dt) / jd2s

print("\nName: {}\ndDelta: {}\nnSteps: {}".format(name, delta, steps))
print("\ndt: {} sec\ntotal time: {} days\n".format(dt,ttot))


FOUT = open("skid_vs_time.dat", "w")

# SKID
for i, f in enumerate(outputs):

    print("{0:05d} / {1}".format(i+1,nout))

    #f0 = f.split("/")[-1]
    f1 = f[0:-4]

    command = "{0} -s 400 -tau 0.06 -hop -o {1} -stats < {1}".format(skid, f1)
    
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

    log = process.communicate()

    nDark = int(str(log[0]).split('\\n')[1].split()[0][6:])
    nGas = int(str(log[0]).split('\\n')[1].split()[1][5:])
    nStar = int(str(log[0]).split('\\n')[1].split()[2][6:])
    unbound = int(str(log[0]).split('\\n')[10].split(':')[-1])

    #print("\nndark: {}\nngas: {}\nnstar: {}\n\nunbound: {}\n".format(nDark, nGas, nStar, unbound))

    FOUT.write("{:05d} {} {} {} {}\n".format(i, nDark, nGas, nStar, unbound))

FOUT.close()
