do for [i=0:268519] {
    j = i
    set terminal qt size 1200,1200
    set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb"black" behind
    set xrange [-10:10]
    set yrange [-10:10]
    set object circle at graph 0.5,0.5 radius char 0.1 fillcolor rgb"#FF8C00" fillstyle solid noborder
    fname = sprintf("Out_mmsn_%012.0f.dat",j)
    plot fname u 5:6 with points ps 0 lt rgb"white"
    set label 1 sprintf("Step %05.0f",j)
}
