#!/usr/bin/env python

from __future__ import print_function

"""Determine first and last outputs.

Determine information about the first and last steps in the run. How many bodies
did we start with and how many did we end with? What are the minimum and maximum
masses and the beginning and end of the run?
"""

__author__ = "Miles Timpe"
__copyright__ = "Copyright 2017, Miles Timpe"
__credits__ = ["Miles Timpe"]
__license__ = "The MIT License"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Developement"

import glob
import matplotlib.pyplot as plt
import numpy as np
import sys
import time

# Where are we looking?
run = "/zbox/user/mtimpe/projects/drazkowska/simulations/A0M060N2noG/r014"

print("\nAve, Imperator!")
print("\nThis script will analyse the Genga run at:\n\n\t{}".format(run))

# Functions
def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

# update_progress() : Displays or updates a console progress bar
def update_progress(progress):
    barLength = 40 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rProgress: [{0}] {1:.1f}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()

# Constants [cgs]
# https://www.cfa.harvard.edu/~dfabricant/huchra/ay145/constants.html
msolar = 1.989e+33  # [g] (solar mass)
#rsolar = 6.9599e+10  # [cm] (equatorial solar radius)
mearth = 5.9736e+27  # [g] (earth mass)
#rearth = 6.37814e+08  # [cm] (equatorial earth radius)
#au = 1.4960e+13  # [cm] (astronomical unit)
#ngc = 6.67259e-08  # [cm3 g-1 s-2] (newton's gravitational constant)

# Glob output files
outputs = glob.glob("{}/Out_*.dat".format(run))
outputs = sorted(outputs, key=lambda name: int(name[-16:-4]))
nsteps = len(outputs)
print("\nThere are {} outputs.\n".format(nsteps))


# First output
initial = outputs[0]
initialstep = int(initial[-16:-4])

# Last output
final = outputs[-1]
finalstep = int(final[-16:-4])

# Values to track
steps = []
nbodies = []
mass_maximum = []

# Iterate over ordered outputs
for i, f in enumerate(outputs):
    step = int(f[-16:-4])
    steps.append(step)

    # Number of bodies at this step
    n = file_len(f)
    nbodies.append(n)

    # Read output file for current step
    with open(f, 'r') as d:
        for j, line in enumerate(d):
            m = float(line.split()[2])
            if j == 0:
                mmax = m
            elif m > mmax:
                mmax = m

    mmax = mmax * msolar / mearth  # earth masses

    mass_maximum.append(mmax)

    update_progress(float(i)/float(nsteps))

    #print("{:>12}{:>6}{:>10.3f}".format(step, n, mmax))


# Plot
fig1, ax1 = plt.subplots(3, 1, sharex=True, facecolor="white")

# Number of bodies
axis = ax1[0]

axis.set_xlabel("step")
axis.set_xlim(initialstep, finalstep)

axis.set_ylabel("N")
axis.set_ylim(0, file_len(initial))

axis.plot(steps, nbodies, '-k',
    lw=3)

# Maximum mass
axis = ax1[1]

axis.set_xlabel("step")
axis.set_xlim(initialstep, finalstep)

axis.set_ylabel(r"$m_{max}$ $(M_{\oplus})$")
axis.set_ylim(0, 20)

axis.plot(steps, mass_maximum, '-k',
    lw=3)

# Show
plt.show()
