#!/usr/bin/env python

from __future__ import print_function

"""Determine first and last outputs.

Determine information about the first and last steps in the run. How many bodies
did we start with and how many did we end with? What are the minimum and maximum
masses and the beginning and end of the run?
"""

__author__ = "Miles Timpe"
__copyright__ = "Copyright 2017, Miles Timpe"
__credits__ = ["Miles Timpe"]
__license__ = "The MIT License"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Developement"

import glob
import matplotlib.pyplot as plt
import numpy as np
import sys
import time

# Where are we looking?
run = "/zbox/user/mtimpe/projects/drazkowska/simulations/A0M060N2noG/r014"

print("\nAve, Imperator!")
print("\nThis script will analyse the Genga run at:\n\n\t{}".format(run))

# Functions
def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

# update_progress() : Displays or updates a console progress bar
def update_progress(progress):
    barLength = 40 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rProgress: [{0}] {1:.1f}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()

# Constants [cgs]
# https://www.cfa.harvard.edu/~dfabricant/huchra/ay145/constants.html
msolar = 1.989e+33  # [g] (solar mass)
#rsolar = 6.9599e+10  # [cm] (equatorial solar radius)
mearth = 5.9736e+27  # [g] (earth mass)
#rearth = 6.37814e+08  # [cm] (equatorial earth radius)
#au = 1.4960e+13  # [cm] (astronomical unit)
#ngc = 6.67259e-08  # [cm3 g-1 s-2] (newton's gravitational constant)

# Glob output files
outputs = glob.glob("{}/Out_*.dat".format(run))
outputs = sorted(outputs, key=lambda name: int(name[-16:-4]))
nsteps = len(outputs)
print("\nThere are {} outputs.\n".format(nsteps))


# Open GNU macro

# Iterate over ordered outputs
for i, f in enumerate(outputs):
    step = int(f[-16:-4])
