#!/usr/bin/env python

"""
Collision statistics and plots.
"""

import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
sys.path.insert(0, "/home/cluster/mtimpe/data/src/G3/Helpers")
import kepler_helpers as kh

#=============================== path to data ===============================#

# collision file(s) path
dataA = "/home/cluster/mtimpe/data/projects/bonati/simulations/A-type/formation/CollisionsA-type.dat"
dataG = "/home/cluster/mtimpe/data/projects/bonati/simulations/G-type/formation/Collisions_Gtype.dat"
dataM = "/home/cluster/mtimpe/data/projects/bonati/simulations/M-type/formation/Collisions_Mtype.dat"

cfiles = [dataA, dataG, dataM]

# get run time

# collision file(s) path
tA = "/home/cluster/mtimpe/data/projects/bonati/simulations/A-type/formation/timeA-type.dat"
tG = "/home/cluster/mtimpe/data/projects/bonati/simulations/G-type/formation/time_Gtype.dat"
tM = "/home/cluster/mtimpe/data/projects/bonati/simulations/M-type/formation/time_Mtype.dat"

tfiles = [tA, tG, tM]

# savefig path
figs = "/home/cluster/mtimpe/figures/"

#=============================== parameters ===============================#

# Initial number of bodies
icA = 1600
icG = 1250
icM = 1000

ictype = [icA, icG, icM]

# reset condition
#r_reset = 1.0  # times the critical radius

#=============================== constants ===============================#
# newton's gravitational constant
ngc = 2.959e-4  # AU3/Msol/day2
# genga unit scaling
gaussk = 0.01720209895
# unit conversions
d2s = 24 * 60 * 60
au2km = 149597870.7  # 1 AU to km
sol2kg = 1.989e30  # solar mass to kg
e2j = 5.961214e42  # numerator to joules
sol2ear = 332948.6  # solar mass to earth masses
AUD_KMS = 1731.46
#=============================== functions ===============================#

def unit_vector(vector):
	""" Returns the unit vector of the vector. """
	return vector / np.sqrt(vector.dot(vector))


def angle_between(v1, v2):
	""" Returns the angle in radians between vectors 'v1' and 'v2'. """
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def point_of_impact(col):
	""" Returns the pre-collision initial conditions. """
	# time in Myr
	t = col[0] / 1.e6

	# particle mass [msun]
	mi = col[2]
	mj = col[14]

	# collision mass ratio
	mratio = max(mi, mj) / min(mi, mj)

	# barycentric positions [AU]
	xi, yi, zi = col[4], col[5], col[6]
	xj, yj, zj = col[16], col[17], col[18]

	p = np.array([xi, yi, zi])
	q = np.array([xj, yj, zj])

	# barycentric velocities [AU/day]
	vxi, vyi, vzi = col[7], col[8], col[9]
	vxj, vyj, vzj = col[19], col[20], col[21]

	u = gaussk * np.array([vxi, vyi, vzi])
	v = gaussk * np.array([vxj, vyj, vzj])

	ucu = np.array([vxi, vyi, vzi])
	vcu = np.array([vxj, vyj, vzj])

	# keplerian orbital elements
	#orbit_i = kh.cart2kep(p, ucu, mi, central_mass=1.0):
	#orbit_j = kh.cart2kep(q, vcu, mj, central_mass=1.0):

	# collision frame (center of mass)
	rCM = (p*mi + q*mj) / (mi + mj)
	# COM positions
	pCM = p - rCM
	qCM = q - rCM
	# COM velocities
	drdtCM = (mi*u + mj*v) / (mi + mj)
	uCM = u - drdtCM
	vCM = v - drdtCM
	# COM radial velocities
	u_rCM = np.dot(pCM,uCM) * 1731.46  # km/s
	v_rCM = np.dot(qCM,vCM) * 1731.46  # km/s

	# pro/retrograde orientation
	hi = np.cross(p,u)
	hj = np.cross(q,v)

	if hi[2] > 0:
		hi = 'prograde'
	elif hi[2] < 0:
		hi = 'retrograde'

	if hj[2] > 0:
		hj = 'prograde'
	elif hj[2] < 0:
		hj = 'retrograde'

	# relative postion [AU]
	r = p - q

	# relative/impact velocity [AU/day]
	w = u - v

	# critical radius [AU]
	ri = col[3]
	rj = col[15]
	rcrit = ri + rj

	# quadratic method
	coeff_a = w.dot(w)
	coeff_b = 2 * w.dot(r)
	coeff_c = r.dot(r) - (rcrit**2)
	# numerical roundabout
	coeff_q = -0.5 * (coeff_b + np.sign(coeff_b) * np.sqrt((coeff_b**2) - (4 * coeff_a * coeff_c)))
	# roots
	t1 = coeff_q / coeff_a
	t2 = coeff_c / coeff_q

	# correct positions
	r1 = r + (t1 * w)
	r2 = r + (t2 * w)

	#=======================================================================#
	# impact parameter using closest point of approach method
	if w.dot(w) < 0.000001:
		t_cpa = 0
	else:
		t_cpa = -np.dot(r,w) / w.dot(w)
	# positions
	p_cpa = p + (t_cpa * u)
	q_cpa = q + (t_cpa * v)
	r_cpa = p_cpa - q_cpa
	# impact parameter
	b = np.sqrt(r_cpa.dot(r_cpa)) / rcrit

	# impact velocity
	vimpact = np.sqrt(w.dot(w)) * 1731.46  # km/s
	m_target = max([mi, mj]) #* 1.988435e30  # target mass in kg
	r_target = max([ri, mj]) #* 1.496e11  # target radius in m
	vesc = np.sqrt(2 * ngc * m_target / r_target)  # AU/day
	#vimpact = np.sqrt(w.dot(w)) / vesc

	# center of mass (corrected)
	r_center = (mi * r1 + mj * r2) / (mi + mj)
	vcom = (mi*u + mj*v) / (mi + mj)
	p_center = r1 - r_center
	q_center = r2 - r_center

	# impact energy
	mu = mi * mj / (mi + mj)  # [msol]
	QR = 0.5 * mu * w.dot(w) / (mi + mj)
	QR_MJ = (QR * (e2j/sol2kg)) / 1.e6  # megajoules per kg

	mu = mu * sol2ear  # reduced mass in Earth masses
	mtot = (mi + mj) * sol2ear  # total mass in Earth masses
	mtar = max([mi, mj]) * sol2ear  # target mass in Earth masses
	mimp = min([mi, mj]) * sol2ear  # impactor mass in Earth masses

	return pd.Series([t, mtar, mimp, mtot, mu, mratio, b, vimpact, QR_MJ, hi, hj, u_rCM, v_rCM],
			['time', 'mtar', 'mimp', 'mtot', 'mu', 'mratio', 'b', 'vimpact', 'QR', 'hi', 'hj', 'u_rCM', 'v_rCM'])


#=============================== read in data ===============================#

sns.set(style="white")

fig1, ax1 = plt.subplots(1,1,figsize=(8,8))

#ax1.set_xlim(0,25)

cmaps = ["Blues", "Greens", "Reds"]

blue = sns.color_palette("Blues")[-2]
green = sns.color_palette("Greens")[-2]
red = sns.color_palette("Reds")[-2]


ax1.text(0.1, 17.5, "Giant Impacts", size=22)

ax1.text(0.65, 14, "A dwarf", size=16, color=blue)
ax1.text(0.8, 9.75, "G dwarf", size=16, color=green)
ax1.text(0.6, 7., "M dwarf", size=16, color=red)

for ax in ['top','bottom','left','right']:
	ax1.spines[ax].set_linewidth(3)

ax1.minorticks_on()

ax1.tick_params('both', length=10, width=3, which='major', labelsize=18)

ax1.tick_params('both', length=5, width=3, which='minor', labelsize=18)

#=============================== read in data ===============================#
print("\t// Reading data...")

for i, data in enumerate(cfiles):

	print("\n\t\tFile: {} ( < 25 Myr )".format(data.split("/")[-1]))

	# read in collision files
	df = pd.read_csv(data, delim_whitespace=True)
	df.columns = ['time', 'indexi', 'mi', 'ri', 'xi', 'yi', 'zi', 'vxi', 'vyi', 'vzi', 'Sxi', 'Syi', 'Szi', 
			'indexj', 'mj', 'rj', 'xj', 'yj', 'zj', 'vxj', 'vyj', 'vzj', 'Sxj', 'Syj', 'Szj']

	# corrected impact geometries
	dfc = df.apply(point_of_impact, axis=1)
	dfc.columns = ['time', 'mtar', 'mimp', 'mtot', 'mu', 'mratio', 'b', 'vimpact', 'QR', 'hi', 'hj', 'u_rCM', 'v_rCM']

	# Filter to exclude all data after 25 Myr
	dfc = dfc[dfc.time < 25.]


	# Filter for only giant impacts
	#print(dfc)
	dfc = dfc[dfc.QR > 0.3]
	dfc = dfc[dfc.mtot > 0.1]


	#=============================== plot stuff ===============================#
	#print("\t// Plotting stuff...")

	#foo = dfc.sort_values(by=['vimpact'], ascending=False).head(50)
	#print(foo)

	# stage data
	x = pd.Series(dfc.b, name=r"Impact Parameter")
	y = pd.Series(dfc.vimpact, name=r"Impact Velocity")

	icn = ictype[i]

	tline = subprocess.check_output(['tail', '-1', tfiles[i]])
	tline = str(tline)[2:-3]
	steps = tline.split()[0]
	tmyrs = int(steps) * 0.5 * 0.00273973 * 1.e-6  # steps -> days -> years -> myrs
	
	print('\n\t\tLast output step: {:.1f} Myr'.format(tmyrs))
	print('\t\tNumber of collisions: {}'.format(len(dfc)))
	print('\t\tInitial number of bodies: {}'.format(icn))
	print('\t\tBodies removed via collisions: {:.1f}%'.format(100. * len(dfc) / icn))
	print('\t\tMinimum Impact Velocity {:.1f} km/s\n\t\tMaximum Impact Velocity {:.1f} km/s\n'.format(min(y), max(y)))


	#cmap = sns.cubehelix_palette(start=i, light=1, as_cmap=True)
	sns.kdeplot(x, y, cmap=cmaps[i], shade=True, shade_lowest=False, alpha=0.8, ax=ax1)
	ax1.set_xlim(0.0, 1.0)
	ax1.set_ylim(0, 20)

	# hex plot
	#g = sns.jointplot(x, y, kind="hex", xlim=(min(x), max(x)), ylim=(min(y), max(y)), stat_func=None, marginal_kws={'color': 'DarkOrange'}, ax=ax1)
	#figure = figs + "Atype_hex.png"
	#sns.jointplot(x, y, kind="kde", stat_func=None, ax=ax1)
	#plt.savefig(figure, format='png', dpi=300)
	#plt.show()
	#plt.clf()


	#x = pd.Series(dfc.time, name=r"Time of Collision (Myr)")
	#sns.distplot(x, bins=25, kde=False, rug=False, ax=ax1)
	#sns.set(style="white")
	#sns.distplot(x, bins=20, kde=False, rug=False)
	#figure = figs + "Atype_toc.png"
	#plt.savefig(figure, format='png', dpi=300)
	#plt.show()
	#plt.clf()

	del dfc

ax1.set_xlabel("Impact Parameter", fontsize=22)
ax1.set_ylabel("Impact Velocity", fontsize=22)

figure = figs + "kdeplot_gi.png"
plt.savefig(figure, format='png', dpi=300)
plt.show()

print("\t// Done!\n")
