#!/usr/bin/env python

import glob
import matplotlib
matplotlib.use('qt5agg')
import matplotlib.pyplot as plt
import numpy as np
import sys

from matplotlib.gridspec import GridSpec
from mpl_toolkits.mplot3d import Axes3D

import kepler_helpers as kepler

# Constants
MSOL2ME = 332948.6  # solar mass
GENGA_VEL_FACTOR = 0.0172020989  # velocity conversion

# Impact data
impact = '0.062725954569857980681 5207 1.5017344530000001005e-06 4.7400724050000003198e-05 \
        -0.97398097886431123094 -0.44753574483570490727 -0.014302192637669889144 \
        0.50271786116828665048 -0.92746542683794708228 0.0064967381941200535306 \
        0 0 0 \
        579 1.5017344530000001005e-06 4.7400724050000003198e-05 \
        -0.97391875868693855711 -0.44748769067611460226 -0.014328023458405222262 \
        0.25451350576683046123 -0.82092076577109296398 -0.035000766295535459816 \
        0 0 0'

# Track these PIDs
pid1 = 5207
pid2 = 579

# Time of impact
toi = 0.062725954569857980681  # years

# Point of impact
poi = np.array([-0.97398097886431123094, -0.44753574483570490727, -0.014302192637669889144])  # AU

outputs = sorted(glob.glob("Out_*.dat"))

#outputs = ['Out_hires_000000000000.dat']

dtype = np.dtype([('t', 'float64'), ('pid', 'int'), \
        ('m', 'float64'), ('r', 'float64'),\
        ('x', 'float64'), ('y', 'float64'), ('z', 'float64'),\
        ('vx', 'float64'), ('vy', 'float64'), ('vz', 'float64')])

# Colors
bc = 'black'  # background color
pc = 'white'  # particle color
tc = 'white'  # text color
c1 = 'orange'    # target color
c2 = 'darkturquoise'   # projectile color

# Axis limit
lim = 3

# Width of cutout
w = 0.015  # AU
ww = w * 10.


for nframe, f in enumerate(outputs):
    print('\n{}'.format(f))

    # Setup figure
    fig = plt.figure(figsize=(15,9), facecolor='black')

    G = GridSpec(3, 5)

    ax = fig.add_subplot(G[:,:3], projection='3d', aspect='equal', axisbg=bc)
    ax1 = fig.add_subplot(G[0,3], projection='3d', aspect='equal', axisbg=bc)
    ax2 = fig.add_subplot(G[0,4], projection='3d', aspect='equal', axisbg=bc)
    ax3 = fig.add_subplot(G[1:,3:], projection='3d', aspect='equal', axisbg=bc)

    ax.set_xlim(-lim,lim)
    ax.set_ylim(-lim,lim)
    ax.set_zlim(-lim,lim)

    # Turn off axes
    ax.axis('off')
    ax1.axis('off')
    ax2.axis('off')
    ax3.axis('off')

    # Load current data
    pts = np.loadtxt(f, dtype=dtype, skiprows=0, usecols=(0,1,2,3,4,5,6,7,8,9))

    # Plot all particles
    ax.scatter(pts['x'], pts['y'], pts['z'], c=pc, marker='.', s=1.0, zorder=9)
    ax1.scatter(pts['x'], pts['y'], pts['z'], c=pc, edgecolor=pc, marker='.', s=10, zorder=9)
    ax2.scatter(pts['x'], pts['y'], pts['z'], c=pc, edgecolor=pc, marker='.', s=10, zorder=9)
    ax3.scatter(pts['x'], pts['y'], pts['z'], c=pc, edgecolor='', marker='.', s=2, zorder=9)

    # Time to impact
    t = pts[0]['t']
    tdays = (toi - t) * 365.  # days
    thours = (tdays - int(tdays)) * 24.  # hours
    tmins = (thours - int(thours)) * 60.  # mins
    tsecs = (tmins - int(tmins)) * 60.  # secs
    #print('Time to impact: {} days {} hrs {} mins {} sec'.format(int(tdays), int(thours), int(tmins), int(tsecs)))

    # Countdown clock
    ax.text2D(0.01, 0.99, 'ESTIMATED TIME TO IMPACT:',
            color=tc, fontsize=8, transform=ax.transAxes)

    ax.text2D(0.01, 0.95, '{:02d}$-${:02d}:{:02d}:{:02d}'
            .format(int(tdays), int(thours), int(tmins), int(tsecs)), color=tc,
            fontsize=18, transform=ax.transAxes)

    ax.text2D(0.01, 0.94, '{}      {}   {}   {}'
            .format('DAYS', 'HOUR', 'MINS', 'SECS'), color=tc,
            fontsize=8, va='top', ha='left', transform=ax.transAxes)

    # Show system information
    ax.text2D(0.01, 0.04, r'$\tau$ Ceti',
            color=tc, fontsize=18, transform=ax.transAxes)

    ax.text2D(0.01, 0.02, '  G-TYPE DWARF STAR\n  STAGE II FORMATION\n  {} PARTICLES'
            .format(len(pts)),
            color=tc, fontsize=7, va='top', ha='left', transform=ax.transAxes)

    # Show computational information
    ax3.text2D(0.85, 0.04, 'CSCS',
            color=tc, fontsize=18, transform=ax3.transAxes)

    ax3.text2D(0.85, 0.02, '  LUGANO, SWITZERLAND\n  CPU: INTEL XEON E5-2690\n  GPU: NVIDIA TESLA P100',
            color=tc, fontsize=7, va='top', ha='left', transform=ax3.transAxes)

    line_x = []
    line_y = []
    line_z = []

    positions = []
    velocities = []

    for pid in [pid1,pid2]:
        idx = np.where( pts['pid'] == pid )

        x = float(pts[idx]['x'])
        y = float(pts[idx]['y'])
        z = float(pts[idx]['z'])

        vx = float(pts[idx]['vx'])
        vy = float(pts[idx]['vy'])
        vz = float(pts[idx]['vz'])

        m = float(pts[idx]['m'])
        r = np.array([x,y,z])
        v = np.array([vx,vy,vz])

        m_mag = m * MSOL2ME               # Earth masses
        r_mag = np.linalg.norm(r)         # AU
        v_mag = np.linalg.norm(v) * 1731. * GENGA_VEL_FACTOR  # km/s

        line_x.append(x)
        line_y.append(y)
        line_z.append(z)

        positions.append([x,y,z])
        velocities.append([vx,vy,vz])

        if pid == pid1:
            # Target
            axis = ax1
            le = 4.
            lte = 4.1
            lc = c1
            lva = 'bottom'
        elif pid == pid2:
            # Projectile
            axis = ax2
            le = -4.0
            lte = -4.1
            lc = c2
            lva = 'top'

        ax.plot([x,x], [y,y], [z,z+le], color=lc, linewidth=0.8,
                label='PID {0:04d}'.format(pid))

        ax.text(x, y, z + lte,
                'PID {:04d}'.format(pid),
                color=lc, fontsize=8,
                va=lva)

        # Cartesian to Keplerian
        a, e, i, bigOm, omega, MA = kepler.cart2kep(r, v, m, central_mass=1.0)

        # Calculate orbital ellipses
        orbit = kepler.compute_ellipse(a, e, i, bigOm, omega)

        orbitx = orbit[0]  #[:30]
        orbity = orbit[1]  #[:30]
        orbitz = orbit[2]  #[:30]


        # Plot orbital ellipse
        ax.plot(orbitx, orbity, orbitz, color=lc, linewidth=2, alpha=0.5)
        ax1.plot(orbitx, orbity, orbitz, color=lc, linewidth=1)  #, alpha=0.5)
        ax2.plot(orbitx, orbity, orbitz, color=lc, linewidth=1)  #, alpha=0.5)
        ax3.plot(orbitx, orbity, orbitz, color=lc, linewidth=1)  #, alpha=0.5)

        # Plot larger point for target and projectile
        ax1.scatter(x, y, z, c=lc, marker='o', s=50, zorder=1)
        ax2.scatter(x, y, z, c=lc, marker='o', s=50, zorder=1)
        ax3.scatter(x, y, z, c=lc, marker='o', s=20, zorder=1)

        # Plot object position and velocity
        axis.text(x, y, z,
                'm = {0:.2f} MEARTH\nr = {1:.5f} AU\nv = {2:.1f} KM/S'
                .format(m_mag, r_mag, v_mag),
                color=tc, fontsize=6, ha='left', va='top')


    # Line connecting bodies
    ax.plot(line_x, line_y, line_z, color='gray', linewidth=0.5, alpha=0.5)
    ax1.plot(line_x, line_y, line_z, color='gray', linewidth=0.5, alpha=0.5)
    ax2.plot(line_x, line_y, line_z, color='gray', linewidth=0.5, alpha=0.5)

    # How far apart are our bodies?
    d = np.linalg.norm(np.array(positions[0]) - np.array(positions[1]))
    vrel = np.linalg.norm(np.array(positions[0]) - np.array(positions[1])) * 1731. * GENGA_VEL_FACTOR
    print('Separation: {:.4f} AU\nRelative velocity: {}'.format(d, vrel))

    # Zoom in cutout
    xcenter = positions[0][0]
    ycenter = positions[0][1]
    zcenter = positions[0][2]

    # Zoom to target
    ax1.set_title('Target / PID {:04d}'.format(pid1), color=tc, fontsize=8)
    ax1.set_xlim(xcenter-w, xcenter+w)
    ax1.set_ylim(ycenter-w, ycenter+w)
    ax1.set_zlim(zcenter-w, zcenter+w)

    # Zoom in cutout
    xcenter = positions[1][0]
    ycenter = positions[1][1]
    zcenter = positions[1][2]

    # Zoom to projectile
    ax2.set_title('Projectile / PID {:04d}'.format(pid2), color=tc, fontsize=8)
    ax2.set_xlim(xcenter-w, xcenter+w)
    ax2.set_ylim(ycenter-w, ycenter+w)
    ax2.set_zlim(zcenter-w, zcenter+w)

    # Zoom to point of impact
    xpoi, ypoi, zpoi = poi[0], poi[1], poi[2]

    ax3.scatter(xpoi, ypoi, zpoi, c='white', marker='^', s=50)
    ax3.set_xlim(xpoi-ww, xpoi+ww)
    ax3.set_ylim(ypoi-ww, ypoi+ww)
    ax3.set_zlim(zpoi-ww, zpoi+ww)

    ax3.text(xpoi, ypoi, zpoi, 'Point of Impact',
            color=tc, fontsize=8, ha='left', va='top')

    # Plot central star
    ax.scatter([0.], [0.], [0.], c='DarkOrange', edgecolor='DarkOrange', marker='o', s=10, zorder=5)
    ax1.scatter([0.], [0.], [0.], c='DarkOrange', edgecolor='DarkOrange', marker='o', s=10, zorder=5)
    ax2.scatter([0.], [0.], [0.], c='DarkOrange', edgecolor='DarkOrange', marker='o', s=10, zorder=5)
    ax3.scatter([0.], [0.], [0.], c='DarkOrange', edgecolor='DarkOrange', marker='o', s=10, zorder=5)

    # Plot radial scale
    ax.plot([0.,5.], [0,0], [0,0], color='gray', linewidth=1, alpha=0.5)
    #ax.plot([5.,5.], [-0.1,0.1], [0,0], color='gray', linewidth=2, alpha=0.5)
    ax.text(5.1, 0., 0., '5 AU', color='gray', fontsize=8, alpha=0.5)

    #plt.subplots_adjust(wspace=0, hspace=0)

    plt.savefig('/home/mtimpe/Desktop/leadup_hi_res/frames/img{:05d}.png'.format(nframe),
            format='png', dpi=500, facecolor='black')

    plt.close(fig)

print('\nDone!\n')
