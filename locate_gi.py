#!/usr/bin/env python

"""Identify giant impacts in collision file.

"""

import numpy as np
import pandas as pd

#=============================== parameters ===============================#

# reset condition
r_reset = 1.0  # times the critical radius

#=============================== constants ===============================#
# newton's gravitational constant
ngc = 2.959e-4  # AU3/Msol/day2
# genga unit scaling
gaussk = 0.01720209895
# unit conversions
d2s = 24 * 60 * 60
au2km = 149597870.7  # 1 AU to km
sol2kg = 1.989e30  # solar mass to kg
e2j = 5.961214e42  # numerator to joules
sol2ear = 332948.6  # solar mass to earth masses
#=============================== functions ===============================#

def unit_vector(vector):
	""" Returns the unit vector of the vector. """
	return vector / np.sqrt(vector.dot(vector))


def angle_between(v1, v2):
	""" Returns the angle in radians between vectors 'v1' and 'v2'. """
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def point_of_impact(col):
	""" Returns the pre-collision initial conditions. """
	# time in Myr
	t = col[0] / 1.e6

	# barycentric positions [AU]
	xi, yi, zi = col[4], col[5], col[6]
	xj, yj, zj = col[16], col[17], col[18]

	p = np.array([xi, yi, zi])
	q = np.array([xj, yj, zj])

	# barycentric velocities [AU/day]
	vxi, vyi, vzi = col[7], col[8], col[9]
	vxj, vyj, vzj = col[19], col[20], col[21]

	u = gaussk * np.array([vxi, vyi, vzi])
	v = gaussk * np.array([vxj, vyj, vzj])

	# relative postion [AU]
	r = p - q

	# relative/impact velocity [AU/day]
	w = u - v

	# critical radius [AU]
	ri = col[3]
	rj = col[15]
	rcrit = ri + rj

	# quadratic method
	coeff_a = w.dot(w)
	coeff_b = 2 * w.dot(r)
	coeff_c = r.dot(r) - (rcrit**2)
	# numerical roundabout
	coeff_q = -0.5 * (coeff_b + np.sign(coeff_b) * np.sqrt((coeff_b**2) - (4 * coeff_a * coeff_c)))
	# roots
	t1 = coeff_q / coeff_a
	t2 = coeff_c / coeff_q

	# correct positions
	r1 = r + (t1 * w)
	r2 = r + (t2 * w)

	#=======================================================================#
	# impact parameter using closest point of approach method
	if w.dot(w) < 0.000001:
		t_cpa = 0
	else:
		t_cpa = -np.dot(r,w) / w.dot(w)
	# positions
	p_cpa = p + (t_cpa * u)
	q_cpa = q + (t_cpa * v)
	r_cpa = p_cpa - q_cpa
	# impact parameter
	b = np.sqrt(r_cpa.dot(r_cpa)) / rcrit

	# particle mass [Msun]
	mi = col[2]
	mj = col[14]
	
        # collision mass ratio
	mratio = max(mi, mj) / min(mi, mj)

	# impact velocity
	vimpact = np.sqrt(w.dot(w)) * 1731.46  # km/s
	m_target = max([mi, mj]) #* 1.988435e30  # target mass in kg
	r_target = max([ri, mj]) #* 1.496e11  # target radius in m
	vesc = np.sqrt(2 * ngc * m_target / r_target)  # AU/day
	#vimpact = np.sqrt(w.dot(w)) / vesc

	# center of mass
	#r_center = (mi * p + mj * q) / (mi + mj)
	#p_center = p - r_center
	#q_center = q - r_center

	# impact energy
	mu = mi * mj / (mi + mj)  # [msol]
	QR = 0.5 * mu * w.dot(w) / (mi + mj)
	QR_MJ = (QR * (e2j/sol2kg)) / 1e6  # megajoules per kg

	mu = mu * sol2ear  # reduced mass in Earth masses
	mtot = (mi + mj) * sol2ear  # total mass in Earth masses
	mtar = max([mi, mj]) * sol2ear  # target mass in Earth masses
	mimp = min([mi, mj]) * sol2ear  # impactor mass in Earth masses

	return pd.Series([t, mtar, mimp, mtot, mu, mratio, b, vimpact, QR_MJ], 
			['time', 'mtar', 'mimp', 'mtot', 'mu', 'mratio', 'b', 'vimpact', 'QR'])

#=============================== path to data ===============================#

f1 = "/home/cluster/mtimpe/data/projects/bonati/simulations/A-type/formation/CollisionsA-type.dat"

#=============================== read in data ===============================#
print("\t// Reading data")

# read in collision files
df = pd.read_csv(f1, delim_whitespace=True)
df.columns = ['time', 'indexi', 'mi', 'ri', 'xi', 'yi', 'zi', 'vxi', 'vyi', 'vzi', 'Sxi', 'Syi', 'Szi', 
		'indexj', 'mj', 'rj', 'xj', 'yj', 'zj', 'vxj', 'vyj', 'vzj', 'Sxj', 'Syj', 'Szj']

# corrected impact geometries
dfc = df.apply(point_of_impact, axis=1)
dfc.columns = ['time', 'mtar', 'mimp', 'mtot', 'mu', 'mratio', 'b', 'vimpact', 'QR']

dfc = dfc.sort_values(by=['mtot'], ascending=False)

print(dfc)
