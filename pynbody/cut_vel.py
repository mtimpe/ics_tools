#!/usr/bin/env python

import numpy as np
import pynbody
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file", help="name of input file")
args = parser.parse_args()

f = pynbody.load(args.file, double_all=True)

f['vel'] = 0

f.write(fmt=pynbody.tipsy.TipsySnap, filename='vzero.std', double_all=True)
