#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import colorsys


# Constants_____________________________________________________________________
# http://www.astro.wisc.edu/~dolan/constants.html

G_CGS = 6.67259e-8  # cm3 g-1 s-2


# Model parameters______________________________________________________________

#densities = [0.7, 1.3, 1.6, 3.9, 5.2, 5.4, 5.5]

densities = [1, 2, 3, 4, 5, 6, 7, 8, 9]

planets = ['Saturn', 'Jupiter/Uranus', 'Neptune', \
    'Mars', 'Venus', 'Mercury', 'Earth' ]

# Maclaurin's function__________________________________________________________

def convert_to_hex(r, g, b):
    red = int(r*255)
    green = int(g*255)
    blue = int(b*255)
    return '#{r:02x}{g:02x}{b:02x}'.format(r=red,g=green,b=blue)


def lefthand(omega):
    '''left-hand side'''
    return omega**2. / (2. * np.pi * G_CGS * rho_bulk)


def righthand(e):
    '''right-hand side'''
    u = 1. - e**2.
    x1 = np.sqrt(u) * (3. - 2. * e**2.) * np.arcsin(e) / e**3.
    x2 = 3. * u / e**2.
    return x1 - x2


def maclaurin(e, rho):
    '''right-hand side'''
    u = 1. - e**2.
    x1 = np.sqrt(u) * (3. - 2. * e**2.) * np.arcsin(e) / e**3.
    x2 = 3. * u / e**2.
    return np.sqrt(2. * np.pi * G_CGS * rho * (x1 - x2))


def e2eps(val):
        # Find nearest value of eccentricity
        idx = (np.abs(ecc - val)).argmin()
        # Return associated epsilon
        return eps[idx]

def log_e2eps(val):
        # Find nearest value of eccentricity
        idx = (np.abs(log_ecc - val)).argmin()
        # Return associated epsilon
        return log_eps[idx]

# Precalculate__________________________________________________________________

# Epsilon from 0 to 1
eps = np.linspace(0, 1, num=1000)

# Calculate eccentricity for each epsilon
ecc = np.array([np.sqrt(x * (2.-x)) for x in eps])

# Remove NaN values
eps = eps[~np.isnan(ecc)]
ecc = ecc[~np.isnan(ecc)]

# logarithmic
log_eps = np.logspace(np.log10(1e-5), np.log10(1), num=1000, endpoint=True)
log_ecc = np.array([np.sqrt(x * (2.-x)) for x in log_eps])
log_eps = log_eps[~np.isnan(log_ecc)]
log_ecc = log_ecc[~np.isnan(log_ecc)]

# Figure________________________________________________________________________

plt.style.use('dark_background')

# Colormap
autumn = cm = plt.get_cmap('autumn')

values = range(len(densities))

cNorm  = colors.Normalize(vmin=0, vmax=values[-1])

scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=autumn)

# Fontsize
label_fontsize = 22

# Figure 1______________________________________________________________________
fig1, axis1 = plt.subplots(1, 1, figsize=(12,8))

# Panel 1
ax1 = axis1

#ax1.grid(True)

# x-axis
x1min = 1e-2
x1max = 1e+1

ax1.set_xlabel('$\Omega$ $\\rm (hr^{-1})$', fontsize=label_fontsize)
ax1.set_xlim(x1min, x1max)
ax1.set_xscale('log')

xticks = [0.01, 0.1, 1, 10]
ax1.set_xticks(xticks)
ax1.set_xticklabels(xticks)

# y-axis
#ax1.set_ylabel('$\epsilon$', fontsize=label_fontsize)
ax1.set_ylabel('$\\rm e$', fontsize=label_fontsize)
ax1.set_ylim(0, 1.1)

# logarithmic
#ax1.set_ylim(1e-3, 1.5)
#ax1.set_yscale('log')

# Top axis
ax2 = ax1.twiny()

#x-axis
x2min = 2 * np.pi / x1min
x2max = 2 * np.pi / x1max

ax2.set_xlabel('$\\rm P_{rot}$ $\\rm (hrs)$', fontsize=label_fontsize)
ax2.set_xlim(x2min, x2max)
ax2.set_xscale('log')

xticks = [100, 10, 1]

ax2.set_xticks(xticks)
ax2.set_xticklabels(xticks)

# Thicker axes
for axis in [ax1, ax2]:
    [i.set_linewidth(2) for i in axis.spines.itervalues()]
    axis.tick_params(axis='both', which='major', pad=10)
    axis.tick_params('both', length=5, width=2, which='major', labelsize=16)
    axis.tick_params('both', length=3, width=1, which='minor')

# Critical eccentricity of Maclaurin spheroid
ax1.axhline(y=0.92992992993, xmin=0, xmax=1,
    linewidth=2, linestyle='--', color='white',
    alpha=0.5)


# Figure 2______________________________________________________________________
fig2, axis2 = plt.subplots(1, 1, figsize=(12,8))

# Select subplot in Figure 2
ax1 = axis2

# x-axis
x1min = 1e-2
x1max = 1e+1

ax1.set_xlabel('$\Omega$ $\\rm (hr^{-1})$', fontsize=label_fontsize)
ax1.set_xlim(x1min, x1max)
ax1.set_xscale('log')

xticks = [0.01, 0.1, 1, 10]
ax1.set_xticks(xticks)
ax1.set_xticklabels(xticks)

# y-axis
ax1.set_ylabel('$\log ( \epsilon )$', fontsize=label_fontsize)
#ax1.set_ylim(0, 1.1)

# logarithmic
ax1.set_ylim(1e-5, 1)
ax1.set_yscale('log')

yticks = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1]
ylabels = [-5, -4, -3, -2, -1, 0]
ax1.set_yticks(yticks)
ax1.set_yticklabels(ylabels)

# Top axis
ax2 = ax1.twiny()

#x-axis
x2min = 2 * np.pi / x1min
x2max = 2 * np.pi / x1max

ax2.set_xlabel('$\\rm P_{rot}$ $\\rm (hrs)$', fontsize=label_fontsize)
ax2.set_xlim(x2min, x2max)
ax2.set_xscale('log')

xticks = [100, 10, 1]

ax2.set_xticks(xticks)
ax2.set_xticklabels(xticks)

# Thicker axes
for axis in [ax1, ax2]:
    [i.set_linewidth(2) for i in axis.spines.itervalues()]
    axis.tick_params(axis='both', which='major', pad=10)
    axis.tick_params('both', length=5, width=2, which='major', labelsize=16)
    axis.tick_params('both', length=3, width=1, which='minor')

# Critical eccentricity of Maclaurin spheroid
ax1.axhline(y=0.632632632633, xmin=0, xmax=1,
    linewidth=2, linestyle='--', color='white',
    alpha=0.5)


# Figure 3______________________________________________________________________
fig3, axis3 = plt.subplots(1, 1, figsize=(12,8))

# Select subplot
ax1 = axis3

# Grid
ax1.grid(True, which='both', linestyle='-', alpha=0.25)

# x-axis
ax1.set_xlabel('$\\rho$ $\\rm (g/cm^3)$', fontsize=label_fontsize)

ax1.set_xlim(0.6, 10)
ax1.set_xscale('log')

xticks = [1, 10]
ax1.set_xticks(xticks)
ax1.set_xticklabels(xticks)

# y-axis
ax1.set_ylabel('$\\rm \Omega_{crit}$ $\\rm (hr^{-1})$', fontsize=label_fontsize)

y1min = 5e-1
y1max = 1e+1

ax1.set_ylim(y1min, y1max)
ax1.set_yscale('log')

yticks = [1, 10]
ax1.set_yticks(yticks)
ax1.set_yticklabels(yticks)

# Top axis
ax2 = ax1.twinx()

#x-axis
y2min = 2 * np.pi / y1min
y2max = 2 * np.pi / y1max

ax2.set_ylabel('$\\rm P_{rot,crit}$ $\\rm (hrs)$', fontsize=label_fontsize)
ax2.set_ylim(y2min, y2max)
ax2.set_yscale('log')

yticks = [10, 1]
ax2.set_yticks(yticks)
ax2.set_yticklabels(yticks)

# Grid
ax2.grid(True, which='both', linestyle='-', color='skyblue', alpha=0.5)

# Thicker axes
for axis in [ax1, ax2]:
    [i.set_linewidth(2) for i in axis.spines.itervalues()]
    axis.tick_params(axis='both', which='major', pad=10)
    axis.tick_params('both', length=5, width=2, which='major', labelsize=16)
    axis.tick_params('both', length=3, width=1, which='minor')


# Main__________________________________________________________________________

# Calculate omega for each eccentricity using Maclaurin's formula
e = np.linspace(0, 1, num=1000)

log_e = np.logspace(np.log10(1e-5), np.log10(1.1), num=1000, endpoint=True)

critical_rate = []

hexcolors = []


for n, rho in enumerate(densities):

    # Z-order
    zn = len(densities)-n

    # Line color
    colorVal = scalarMap.to_rgba(values[n])

    # Calculate rotation rates
    omega = np.array([3600. * maclaurin(x, rho) for x in e])

    log_omega = np.array([3600. * maclaurin(x, rho) for x in log_e])

    # Remove NaN values from both arrays
    eccen = e[~np.isnan(omega)]
    omega = omega[~np.isnan(omega)]

    #epsilon = np.array([np.sqrt(x * (2.-x)) for x in eccen])
    epsilon = np.array([e2eps(x) for x in eccen])

    # logarithmic
    log_eccen = log_e[~np.isnan(log_omega)]
    log_omega = log_omega[~np.isnan(log_omega)]
    log_epsilon = np.array([log_e2eps(x) for x in log_eccen])

    #epsilon = epsilon[~np.isnan(epsilon)]
    #log_eccen = log_e[~np.isnan(epsilon)]

    # Calculate rotation rates in Hertz
    #hertz = np.array([maclaurin(x, rho) for x in e])
    #hertz = hertz[~np.isnan(hertz)]

    # Plot
    axis1.plot(omega, eccen, color=colorVal, linewidth=2,
        zorder=zn, label=rho)

    axis2.plot(log_omega, log_epsilon, color=colorVal, linewidth=2,
        zorder=zn, label=rho)

    axis3.scatter([], [], color=colorVal, label=rho)

    #ax2.plot(prot, eccen, c=colorVal, lw=2)

    # Determine critcal rotation rate
    idx = np.argmax(omega)

    # Critical eccentricity
    e_crit = eccen[idx]

    # Critical flattening
    eps_crit = epsilon[idx]

    # Critical rotation rate
    omega_crit = omega[idx]

    # Get hex color code
    r, g, b, alpha = colorVal[0], colorVal[1], colorVal[2], colorVal[3]

    hexcolors.append(convert_to_hex(r, g, b))

    critical_rate.append([rho, omega_crit])

    # Print critcal values
    print('\nDensity: {:>19.2f}{:>10}\
        \nCritical eccentricity:  {:.4f} \
        \nCritical epsilon: {:>12.4f} \
        \nCritical rotation rate: {:.4f}{:>8}'
        .format(rho, 'g/cm3', e_crit, eps_crit, omega_crit, 'rad/d'))


# Figure 3
critical_rate = np.array(critical_rate)

axis3.plot(critical_rate[:,0], critical_rate[:,1],
    color='white', linewidth=2, alpha=0.5, zorder=1)

axis3.scatter(critical_rate[:,0], critical_rate[:,1],
    color=hexcolors, zorder=9)


# Legends
leg1 = axis1.legend(loc=4, title='$\\rho$ (g/cm$^3$)',
    fontsize=18, frameon=False)

leg2 = axis2.legend(loc=4, title='$\\rho$ (g/cm$^3$)',
    fontsize=18, frameon=False)

leg3 = axis3.legend(loc=2, title='$\\rho$ (g/cm$^3$)',
    scatterpoints=1, fontsize=18, frameon=False)

plt.setp(leg1.get_title(), fontsize='x-large')
plt.setp(leg2.get_title(), fontsize='x-large')
plt.setp(leg3.get_title(), fontsize='x-large')

# Save figures
ext = 'svg'
dpi = 1200

fig1.savefig('/Users/enkidu/Desktop/maclaurin_ecc.{}'.format(ext),
    format='{}'.format(ext), dpi=dpi)

fig2.savefig('/Users/enkidu/Desktop/maclaurin_eps.{}'.format(ext),
    format='{}'.format(ext), dpi=dpi)

fig3.savefig('/Users/enkidu/Desktop/maclaurin_crit.{}'.format(ext),
    format='{}'.format(ext), dpi=dpi)

plt.show()
