#!/usr/bin/env python

from __future__ import print_function

import glob
import matplotlib.pyplot as plt
import numpy as np
import pynbody
import scipy
import sys


# Constants
# http://www.astro.wisc.edu/~dolan/constants.html

G_CGS = 6.67259e-8  # cm3 g-1 s-2
AU_CGS = 1.496e13  # cm


# Matplotlib
plt.rc('axes', linewidth=2)


# Maclaurin's function
def Maclaurin(x):
    x0 = 1. - x**2.
    x1 = np.sqrt(x0) * 2. * (3. - 2. * x**2.) * np.arcsin(x) / x**3.
    x2 = 6. * x0 / x**2.
    return x1 - x2


'''
eps0 = 1. / 294.

e0 = np.sqrt(2. * eps0 * (1. - eps0))

r = scipy.optimize.newton(Maclaurin, e0)

print('Root: {}'.format(r))

sys.exit()
'''

# Desired rotation
omega = 2. * np.pi / 86400.  # radians per sec


stages = ['relax', 'spinup', 'damped']

foo = {
        'relax': {
            'x':[],
            'y':[],
            'xmed':[],
            'ymed':[],
            'zmed':[]
            },
        'spinup':{
            'x':[],
            'y':[],
            'xmed':[],
            'ymed':[],
            'zmed':[]
            },
        'damped':{
            'x':[],
            'y':[],
            'xmed':[],
            'ymed':[],
            'zmed':[]
            }
        }


#shift = [0, 100, 1000]
shift = [0, 100, 123]

colors = ['black', 'red', 'blue']


for j, stage in enumerate(stages):

    print(stage)

    # Glob outputs
    steps = glob.glob('{}.*'.format(stage))


    if stage == 'relax':
        steps = [name for name in steps if len(name) == 11]

    else:
        steps = [name for name in steps if len(name) == 12]


    steps = sorted(steps, key=lambda name: int(name[-5:]))

    nsteps = len(steps)

    n = []

    xm = []
    ym = []
    zm = []

    ellipticity = []

    for i, step in enumerate(steps):

        if step == '{}.log'.format(stage):
            next

        # Read in data with pynbody
        f1 = pynbody.load(step)

        # Convert internal units to physical units
        f1['pos'] = f1['pos'].in_units('km')
        f1['vel'] = f1['vel'].in_units('km s**-1')
        f1['rho'] = f1['rho'].in_units('g cm**-3')

        # Maximum radius for x,y,z
        #xmax = max(f1['pos'][:,0])
        #ymax = max(f1['pos'][:,1])
        #zmax = max(f1['pos'][:,2])

        # Returns ascending order
        xmax = np.median(np.sort(f1['pos'][:,0])[-10:])
        ymax = np.median(np.sort(f1['pos'][:,1])[-10:])
        zmax = np.median(np.sort(f1['pos'][:,2])[-10:])

        xmed = np.median(np.sort(f1['pos'][:,0]))
        ymed = np.median(np.sort(f1['pos'][:,1]))
        zmed = np.median(np.sort(f1['pos'][:,2]))

        xm.append(xmed)
        ym.append(ymed)
        zm.append(zmed)

        xmax -= xmed
        ymax -= ymed
        zmax -= zmed


        print('\nxmed = {}\nymed = {}\nzmed = {}\n'.format(xmed, ymed, zmed))

        # Calculate flattening
        a = float(np.mean([xmax,ymax]))
        b = float(zmax)

        # Calculate ellipticity
        epsilon = (a-b)/a

        #print('Maximums (Earth radii)')
        print('x = {}\ny = {}\nz = {}\na = {}\nb = {}\n'.format(xmax,ymax,zmax, a, b))

        # Cause
        #m = omega * R**3. / (G * M)

        # Append for x-axis
        n.append(int(step[-5:])+shift[j])

        # Append for y-axis
        ellipticity.append(epsilon)

        # How far are we along?
        progress = float(i) / float(nsteps)

        print('File: {}, Particles: {}, Flattening: {:.3f},\t\t{:.2f}%'\
        .format(step, len(f1), epsilon, progress*100.))

    if stage == 'damped':
        n.insert(0, foo['spinup']['x'][-1])
        ellipticity.insert(0, foo['spinup']['y'][-1])


    foo[stage]['x'] = n
    foo[stage]['y'] = ellipticity

    foo[stage]['xmed'] = xm
    foo[stage]['ymed'] = ym
    foo[stage]['zmed'] = zm



# Calculate the left side of Maclaurin's formula
mp = 5.976e27  # g

rp = 6.378e8  # cm

rho = 3. * mp / (4. * np.pi * rp**3.)  # g cm-3

h = omega**2. / (np.pi * G_CGS * rho)


# Find the eccentricity using Maclaurin's formula
x1 = np.linspace(0, 1, num=10000)

y1 = np.array([Maclaurin(x) for x in x1])

x1 = x1[~np.isnan(y1)]

y1 = y1[~np.isnan(y1)]

idx = (np.abs(y1-h)).argmin()

ecc = x1[idx]

lhs = y1[idx]

print('\nlhs: {} (analytical)\nidx: {}\necc: {} (numerical)\nlhs: {} (numerical)'.format(h, idx, ecc, lhs))


# Now find epsilon
x2 = np.linspace(0.5, 1., num=10000)  # epsilon

y2 = np.array([np.sqrt(2.* x * (1.-x)) for x in x2])  # eccentricity

x2 = x2[~np.isnan(y2)]

y2 = y2[~np.isnan(y2)]

idx = (np.abs(y2-ecc)).argmin()

eps = x2[idx]

ecc2 = y2[idx]

print('\necc: {}\nidx: {}\neps: {}'.format(ecc, idx, eps))



# Four-panel figure
fig, ax = plt.subplots(2,2,figsize=(16,12), facecolor='white')


# Figure 1
#fig1, ax1 = plt.subplots(1,1,figsize=(6,6), facecolor='white')

axis = ax[0,0]
axis.set_xlabel('step', fontsize=22)
axis.set_ylabel(r'ellipticity ($\epsilon$)', fontsize=22)

for i, stage in enumerate(stages):
    x = foo[stage]['x']
    y = foo[stage]['y']

    c = colors[i]

    axis.plot(x, y, linestyle='-', linewidth=2, color=c, label=stage)

#axis.plot(n, [ecc]*len(x), ':k')

axis.axhline(y=ecc, xmin=0, xmax=1100, color='black')

axis.axhline(y=ecc*0.9, xmin=0, xmax=1100, color='gray')

axis.legend(loc=0)

# Figure 2
#fig2, ax2 = plt.subplots(1,1,figsize=(6,6), facecolor='white')

axis = ax[1,1]
axis.set_xlabel('eccentricity (e)', fontsize=22)
axis.set_ylabel('h', fontsize=22)
axis.set_xlim(0, 2.*ecc)
axis.set_ylim(0, 2.*h)

axis.plot(x1, y1, '-b', x1, [h]*len(x1), ':k')

axis.plot([ecc], [lhs], 'or')


# Figure 3
#fig3, ax3 = plt.subplots(1,1,figsize=(6,6), facecolor='white')

axis = ax[1,0]
axis.set_xlabel(r'ellipticity ($\epsilon$)', fontsize=22)
axis.set_ylabel('eccentricity (e)', fontsize=22)
axis.set_xlim(0.0, 2.*eps)
axis.set_ylim(0.0, 0.2)

axis.plot(x2, y2, '-b', x2, [ecc]*len(x2), ':k')

axis.plot([eps], [ecc2], 'or')


# Figure 4
# Center of mass position
#fig4, ax4 = plt.subplots(1,1,figsize=(6,6), facecolor='white')

axis = ax[0,1]
axis.set_xlabel('step', fontsize=22)
axis.set_ylabel('Distance', fontsize=22)

for i, stage in enumerate(stages):

    if stage == 'damped':
        n = foo[stage]['x'][1:]
    else:
        n = foo[stage]['x']

    x = foo[stage]['xmed']
    y = foo[stage]['ymed']
    z = foo[stage]['zmed']

    if stage == 'spinup':
        axis.plot(n, x, '-b', alpha=0.5, lw=2, label='x')
        axis.plot(n, y, '-r', alpha=0.5, lw=2, label='y')
        axis.plot(n, z, '-g', alpha=0.5, lw=2, label='z')
    else:
        axis.plot(n, x, '-b', alpha=0.5, lw=2)
        axis.plot(n, y, '-r', alpha=0.5, lw=2)
        axis.plot(n, z, '-g', alpha=0.5, lw=2)

axis.legend(loc=3)

fontsize = 18

for axis in [ax[0,0], ax[0,1], ax[1,0], ax[1,1]]:
    #for side in ['top','bottom','left','right']:
    #    axis.spines[side].set_linewidth(3.0)

    for tick in axis.xaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)
        #tick.label1.set_fontweight('bold')
    for tick in axis.yaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)
        #tick.label1.set_fontweight('bold')

fig.tight_layout()

# Show
plt.savefig('/home/mtimpe/Desktop/flattening_segmented.png', format='png')

plt.show()
