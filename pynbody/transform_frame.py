#!/usr/bin/env python

import numpy as np
import pynbody
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("file", help="name of input file")
parser.add_argument("omega", help="angular velocity (rot/day)")
parser.add_argument("angle_x", help="rotation about x-axis (degrees)")
parser.add_argument("angle_y", help="rotation about y-axis (degrees)")
parser.add_argument("angle_z", help="rotation about z-axis (degrees)")
args = parser.parse_args()



f = pynbody.load(args.file)


# Timestep in Gasoline?
rate = float(args.omega) * 2. * np.pi / (86400.)

omega = np.array([0, 0, rate])

foo = np.cross(omega, f['pos'].in_units('km'))

f['vel'] = pynbody.array.SimArray(foo, "km s**-1")


#f.rotate_x(args.angle_x)
#f.rotate_y(args.angle_y)
#f.rotate_z(args.angle_z)

f.write(fmt=pynbody.tipsy.TipsySnap, filename='test.std', double_all=True)
