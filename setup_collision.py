#!/usr/bin/env python

from __future__ import print_function

"""Setup a collision in Gasoline using a line from a Genga collision ouput file.
"""

__author__ = "Miles Timpe"
__copyright__ = "Copyright 2017, Miles Timpe"
__credits__ = ["Miles Timpe"]
__license__ = "The MIT License"
__version__ = "1.0.0"
__maintainer__ = "Miles Timpe"
__email__ = "mtimpe@physik.uzh.ch"
__status__ = "Developement"


import numpy as np


