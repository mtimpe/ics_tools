"""
Helpers for Formation Simulations.
"""

import numpy as np


def return_sources(pid, dfc):
    """
    Construct List of Mass Sources for a given Particle ID.
    Can be used to build Merger Trees.

    @param pid: Particle ID - [Integer]
    @param dfc: Collision list - [Pandas Dataframe from Io_Helpers] 
    @return sources: Source Particle IDs - [Numpy Array]
    """
    dfc = dfc.sort_values(by="time", ascending=True)
    sources = [pid]
    for irow in range(len(dfc)):
        irow = len(dfc)-irow-1
        dfc_loc = dfc.iloc[irow]
        if dfc_loc.pidi in sources:
            sources.append(int(dfc_loc.pidj))
        elif dfc_loc.pidj in sources:
            sources.append(int(dfc_loc.pidi))
    sources = np.asarray(sources, dtype=np.int64)
    return sources


def return_bracket(pid, endtime, dfc):
    """
    Construct collision merger bracket of from sources for a given Particle ID.
    Can be used to build merger trees.

    @param pid: Particle ID - [Integer]
    @param dfc: Collision list - [Pandas Dataframe from Io_Helpers] 
    @return sources: Source Particle IDs - [Numpy Array]
    """
    dfc = dfc.sort_values(by="time", ascending=True)
    sources = [pid]
    bracket = [[float(endtime), int(pid), np.NaN]]
    for irow in range(len(dfc)):
        irow = len(dfc)-irow-1
        dfc_loc = dfc.iloc[irow]

        if dfc_loc.pidi in sources:
            sources.append(int(dfc_loc.pidj))
        elif dfc_loc.pidj in sources:
            sources.append(int(dfc_loc.pidi))

	if (dfc_loc.pidi in sources) or (dfc_loc.pidj in sources):
		bracket.append([float(dfc_loc.time), int(dfc_loc.pidi), int(dfc_loc.pidj)])

    #sources = np.asarray(sources, dtype=np.int64)
    #bracket = np.asarray(bracket)
    return bracket
