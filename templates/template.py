#!/usr/bin/env python

"""Template header for python source.

PEP 257 is followed. Ideally, the authorship information below should be in a
separate LICENSE file unless the code is a standalone.
"""

__author__ = "Rob Knight, Gavin Huttley, and Peter Maxwell"
__copyright__ = "Copyright 2007, The Cogent Project"
__credits__ = ["Rob Knight", "Peter Maxwell", "Matthew Wakefield"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Rob Knight"
__email__ = "rob@spot.colorado.edu"
__status__ = "Production"

from __future__ import print_function
