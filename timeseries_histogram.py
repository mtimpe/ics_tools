#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


xaxis = 'log'

# Load timeseries data
with open('timeseries.dat', 'r') as f:
    data = f.readlines()

data = np.array(data, dtype=float)

xmin = 1e5
xmax = 2e8
nbins = 23


# Plot
sns.set(style="white", palette="muted", color_codes=True)

fig, ax = plt.subplots(1, 1, figsize=(30,3), facecolor='white')

# Load color codes
#sns.set_color_codes("pastel")


ax.set_xlabel('Time (Myr)', fontsize=22)


if xaxis == 'log':
    ax.set_xscale('log')
    ax.set_xlim(xmin,xmax)
    #ax.set_xticks([1e4,1e5,1e6,1e7])
    #ax.set_xticklabels([0.01, 0.1, 1, 10], fontsize=18)

    # Logarithmic bins
    bins = 10**np.linspace(np.log10(xmin), np.log10(xmax), num=nbins)

else:
    ax.set_xlim(0,xmax)
    #ax.set_xticks([1e6,2e6,3e6,4e6,5e6,6e6,7e6,8e6,9e6,1e7])
    #ax.set_xticklabels([1,2,3,4,5,6,7,8,9,10], fontsize=18)
    # Linear bins
    bins = np.linspace(xmin, xmax, num=nbins)


#ax.hist(data, bins=bins, color='#3498db')

sns.distplot(data, bins=bins, kde=False, rug=False,
            axlabel=True, color='b')

sns.despine(left=True, bottom=True)

plt.setp(ax, yticks=[])

plt.savefig('/home/mtimpe/Desktop/timeseries_hist.png', format='png')

plt.show()
