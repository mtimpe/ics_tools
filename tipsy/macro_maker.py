#!/usr/bin/env python

import glob
import sys

# Parameters

plane = 'x'

# Files

files = glob.glob('collision.*')

steps = [name for name in files
	if len(name) == 15]

total = len(steps)

steps = sorted(steps, key=lambda name: int(name[-5:]))

with open('{}macro'.format(plane), 'w') as f:
	f.write('movie\n')

	for i, S in enumerate(steps):
		print('{:05d}\t{}'.format(i, S))
		f.write('openb {}\n'.format(S))
		f.write('loads 0\n')
		f.write('redshift 0 physical 0 1 0 vacuum 1\n')
		f.write('{}all\n'.format(plane))
		f.write('readarray {}.den\n'.format(S))
		f.write('setbox 1 0 0 0 10 10 10\n')
		f.write('abox 1\n')
		f.write('{}array gas revrain -1 7.5\n'.format(plane))
		f.write('hard movie out.xwd\n')
		f.write('shell xwdtopnm < out.xwd | pnmtopng > IMG{:05d}.{}.png\n'.format(i,plane))
		f.write('closeb\n')

	f.write('end\n')
